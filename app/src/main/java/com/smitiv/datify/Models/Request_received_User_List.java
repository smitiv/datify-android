package com.smitiv.datify.Models;

/**
 * Created by govindaraj on 21/05/18.
 */

public class Request_received_User_List {

    public Request_received_User_List(){}

    public String from_fb_id;
    public String fullname;
    public String email;
    public String profilepicture;
    public String gender;
    public String dob;
    public String job_position;
    public String age;

    public Request_received_User_List(String from_fb_id, String fullname, String email, String profilepicture,
                                      String gender, String dob, String job_position, String age) {
        this.from_fb_id = from_fb_id;
        this.fullname = fullname;
        this.email = email;
        this.profilepicture = profilepicture;
        this.gender = gender;
        this.dob = dob;
        this.job_position = job_position;
        this.age = age;
              }

    public String getFrom_fb_id() {
        return from_fb_id;
                }
    public void setFrom_fb_id(String from_fb_id) {
        this.from_fb_id = from_fb_id;
                }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public void setProfilepicture(String profilepicture) {
        this.profilepicture = profilepicture;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getJob_position() {
        return job_position;
    }

    public void setJob_position(String job_position) {
        this.job_position = job_position;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

}
