package com.smitiv.datify.Models;

/**
 * Created by govindaraj on 23/05/18.
 */

public class Chat_Details_List {

    public Chat_Details_List(){}
    public String chat_user_id;
    public String chat_user_name;
    public String chat_user_photo;
    public String chat_message;
    public String chat_date;
    public String chat_time;
    public String chat_status;

    public Chat_Details_List(String chat_user_id, String chat_user_name, String chat_user_photo, String chat_message,
                             String chat_date, String chat_time, String chat_status) {
        this.chat_user_id = chat_user_id;
        this.chat_user_name = chat_user_name;
        this.chat_user_photo = chat_user_photo;
        this.chat_message = chat_message;
        this.chat_date = chat_date;
        this.chat_time = chat_time;
        this.chat_status = chat_status;
                }

    public String getChat_user_id() {
        return chat_user_id;
    }

    public void setChat_user_id(String chat_user_id) {
        this.chat_user_id = chat_user_id;
    }

    public String getChat_user_name() {
        return chat_user_name;
    }

    public void setChat_user_name(String chat_user_name) {
        this.chat_user_name = chat_user_name;
    }

    public String getChat_user_photo() {
        return chat_user_photo;
    }

    public void setChat_user_photo(String chat_user_photo) {
        this.chat_user_photo = chat_user_photo;
    }

    public String getChat_message() {
        return chat_message;
    }

    public void setChat_message(String chat_message) {
        this.chat_message = chat_message;
    }

    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

    public String getChat_status() {
        return chat_status;
                }
    public void setChat_status(String chat_status) {
        this.chat_status = chat_status;
                }

}
