package com.smitiv.datify.Models;

/**
 * Created by govindaraj on 23/05/18.
 */

public class Chat_Receive_List {

    public Chat_Receive_List(){}

    public String chat_from_fb_id;
    public String chat_to_fb_id;
    public String chat_message;
    public String chat_date;
    public String chat_time;

    public Chat_Receive_List(String chat_from_fb_id, String chat_to_fb_id, String chat_message,
                             String chat_date, String chat_time) {
        this.chat_from_fb_id = chat_from_fb_id;
        this.chat_to_fb_id = chat_to_fb_id;
        this.chat_message = chat_message;
        this.chat_date = chat_date;
        this.chat_time = chat_time;
                }

    public String getChat_from_fb_id() {
        return chat_from_fb_id;
    }

    public void setChat_from_fb_id(String chat_from_fb_id) {
        this.chat_from_fb_id = chat_from_fb_id;
    }

    public String getChat_to_fb_id() {
        return chat_to_fb_id;
    }

    public void setChat_to_fb_id(String chat_to_fb_id) {
        this.chat_to_fb_id = chat_to_fb_id;
    }

    public String getChat_message() {
        return chat_message;
    }

    public void setChat_message(String chat_message) {
        this.chat_message = chat_message;
    }

    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

}
