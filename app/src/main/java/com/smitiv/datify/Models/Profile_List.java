package com.smitiv.datify.Models;

/**
 * Created by govindaraj on 17/05/18.
 */

public class Profile_List {

    public Profile_List(){}

    public String id;
    public String fb_id;
    public String fullname;
    public String email;
    public String profilepicture;
    public String gender;
    public String dob;
    public String looking_for;
    public String distance;
    public String alerts;
    public String about_me;
    public String education;
    public String marital_status;
    public String interest;
    public String location;
    public String job_position;
    public String age;
    public String weight;
    public String height;
    public String cover_photo_link;

    public Profile_List(String id, String fb_id, String fullname, String email, String profilepicture,
                        String gender, String dob, String looking_for, String distance, String alerts, String about_me,
                        String education, String marital_status, String interest, String location, String job_position,
                        String age, String weight, String height, String cover_photo_link) {
        this.id = id;
        this.fb_id = fb_id;
        this.fullname = fullname;
        this.email = email;
        this.profilepicture = profilepicture;
        this.gender = gender;
        this.dob = dob;
        this.looking_for = looking_for;
        this.distance = distance;
        this.alerts = alerts;
        this.about_me = about_me;
        this.education = education;
        this.marital_status = marital_status;
        this.interest = interest;
        this.location = location;
        this.job_position = job_position;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.cover_photo_link = cover_photo_link;
                            }

    public String getCover_photo_link() {
        return cover_photo_link;
                }
    public void setCover_photo_link(String cover_photo_link) {
        this.cover_photo_link = cover_photo_link;
                }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public void setProfilepicture(String profilepicture) {
        this.profilepicture = profilepicture;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLooking_for() {
        return looking_for;
    }

    public void setLooking_for(String looking_for) {
        this.looking_for = looking_for;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getAlerts() {
        return alerts;
    }

    public void setAlerts(String alerts) {
        this.alerts = alerts;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getJob_position() {
        return job_position;
    }

    public void setJob_position(String job_position) {
        this.job_position = job_position;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

}
