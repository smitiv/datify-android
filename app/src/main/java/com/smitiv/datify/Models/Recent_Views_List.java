package com.smitiv.datify.Models;

/**
 * Created by govindaraj on 22/05/18.
 */

public class Recent_Views_List {

    public Recent_Views_List(){}

    public String user_fb_id;
    public String fullname;
    public String email;
    public String profilepicture;
    public String job_position;
    public String age;
    public String viewed_date;

    public Recent_Views_List(String user_fb_id, String fullname, String email, String profilepicture,
                             String job_position, String age, String viewed_date) {
        this.user_fb_id = user_fb_id;
        this.fullname = fullname;
        this.email = email;
        this.profilepicture = profilepicture;
        this.job_position = job_position;
        this.age = age;
        this.viewed_date = viewed_date;
                }


    public String getUser_fb_id() {
        return user_fb_id;
    }

    public void setUser_fb_id(String user_fb_id) {
        this.user_fb_id = user_fb_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public void setProfilepicture(String profilepicture) {
        this.profilepicture = profilepicture;
    }

    public String getJob_position() {
        return job_position;
    }

    public void setJob_position(String job_position) {
        this.job_position = job_position;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getViewed_date() {
        return viewed_date;
    }

    public void setViewed_date(String viewed_date) {
        this.viewed_date = viewed_date;
    }

}
