package com.smitiv.datify.Models;

/**
 * Created by navneet on 20/11/16.
 */

public class Data {

    private String name;
    private String imagePath;
    private String age;
    private String designation;
    private String place;
    private String description;

    public Data(String name, String imagePath, String age, String designation, String place, String description) {
        this.name = name;
        this.imagePath = imagePath;
        this.age = age;
        this.designation = designation;
        this.place = place;
        this.description = description;
                }

    public Data(String imagePath, String description) {
        this.imagePath = imagePath;
        this.description = description;
                }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

}
