package com.smitiv.datify.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Dashboard_Classes.Dashboard;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Request_received_User_List;
import com.smitiv.datify.Models.Request_sent_User_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Common_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Request_received_Fragment extends Fragment implements Intent_Constants{

    ArrayList<Request_received_User_List> request_received_list;
    RecyclerView received_recyclerview;
    RecyclerView.Adapter request_received_adapter;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;
    Retrofit retrofit_obj;
    String mess;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    RelativeLayout no_data_layout;

    public Request_received_Fragment() {
            }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_received_layout, container, false);
        preference_init();
        init_view(view);
        return view;
                }

    private void preference_init() {
        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id","");
        str_user_name = pref.getString("datify_name","");
        str_user_email = pref.getString("datify_email","");
        str_user_profile_img = pref.getString("datify_profile_img","");
        str_user_dob = pref.getString("datify_dob","");
        editor.commit();
                }

    private void init_view(View view) {
        no_data_layout = (RelativeLayout) view.findViewById(R.id.no_data_layout);
        received_recyclerview = (RecyclerView) view.findViewById(R.id.received_recyclerview);
        request_received_list = new ArrayList<Request_received_User_List>();
        request_received_list = Stored_datas.getInstance().getRequest_received_list();

        if(request_received_list != null) {
            no_data_layout.setVisibility(View.GONE);
            if (request_received_list.size() > 0) {
                received_recyclerview.setHasFixedSize(true);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());    // LinearLayout for Listview
                received_recyclerview.setLayoutManager(manager);
                request_received_adapter = new Request_received_Adapter(getActivity(), request_received_list);
                received_recyclerview.setAdapter(request_received_adapter);
                request_received_adapter.notifyDataSetChanged();
                no_data_layout.setVisibility(View.GONE);
                            } else {
                no_data_layout.setVisibility(View.VISIBLE);
                            }
                        } else {
            no_data_layout.setVisibility(View.VISIBLE);
                        }
                    }

    class Request_received_Adapter extends RecyclerView.Adapter<Request_received_Adapter.ViewHolder> {
        Context context;
        ArrayList<Request_received_User_List> request_received_list;

        public Request_received_Adapter(Context context,  ArrayList<Request_received_User_List> request_received_list) {
            this.context = context;
            this.request_received_list = request_received_list;
        }
        @Override
        public Request_received_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_received_items, parent, false);
            return new Request_received_Adapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(Request_received_Adapter.ViewHolder holder, final int position) {
            holder.r_received_name.setText(request_received_list.get(position).getFullname());

            if(request_received_list.get(position).getJob_position().equalsIgnoreCase("")) {
                holder.r_received_age.setText("Age: "+request_received_list.get(position).getAge());
                        } else {
                holder.r_received_age.setText(request_received_list.get(position).getAge() + ", ");
                    }

            holder.r_received_designation.setText(request_received_list.get(position).getJob_position());
            Glide.with(context).load(request_received_list.get(position).getProfilepicture()).into(holder.r_received_user_image);

            holder.accept_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String to_fb_id = request_received_list.get(position).getFrom_fb_id();
                    cd = new ConnectionDetector(getActivity());
                    isInternetPresent = cd.isConnectingToInternet();
                    if (!isInternetPresent) {
                        Snackbar.make(received_recyclerview, "No Internet Connection", Snackbar.LENGTH_LONG).
                                setAction("Action", null).show();
                        return;
                            }

                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new request_update(to_fb_id, str_user_fb_id, "A", position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                         }
                    });

            holder.reject_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String to_fb_id = request_received_list.get(position).getFrom_fb_id();
                    cd = new ConnectionDetector(getActivity());
                    isInternetPresent = cd.isConnectingToInternet();
                    if (!isInternetPresent) {
                        Snackbar.make(received_recyclerview, "No Internet Connection", Snackbar.LENGTH_LONG).
                                setAction("Action", null).show();
                        return;
                            }

                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new request_update(to_fb_id, str_user_fb_id, "R", position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                         }
                     });
                }
        @Override
        public int getItemCount() {
            return request_received_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView r_received_user_image;
            TextView r_received_name;
            TextView r_received_age, r_received_designation;
            LinearLayout accept_layout, reject_layout;

            public ViewHolder(View itemView) {
                super(itemView);
                r_received_name = (TextView)itemView.findViewById(R.id.r_received_name);
                r_received_age = (TextView)itemView.findViewById(R.id.r_received_age);
                r_received_designation = (TextView)itemView.findViewById(R.id.r_received_designation);
                r_received_user_image = (ImageView)itemView.findViewById(R.id.r_received_user_image);

                accept_layout = (LinearLayout)itemView.findViewById(R.id.accept_layout);
                reject_layout = (LinearLayout)itemView.findViewById(R.id.reject_layout);
                            }
                        }
                    }

    private class request_update extends AsyncTask<Void, Common_Response, Common_Response> {

        AlertDialog dialog = new SpotsDialog(getActivity());
        String my_fb_id, to_fb_id, status;
        int position;

        public request_update(String my_fb_id, String to_fb_id, String status, int position) {
            this.my_fb_id = my_fb_id;
            this.to_fb_id = to_fb_id;
            this.status = status;
            this.position = position;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Common_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Common_Response> call = a.request_results(my_fb_id, to_fb_id, status);
            Common_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Request_Result_Response: " + c);
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
                }

        @Override
        protected void onPostExecute(Common_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), c.msg, Toast.LENGTH_SHORT).show();
                    Log.e(DATIFY_LOG, "Removed_Person: " + request_received_list.get(position).getFullname());
                    request_received_list.remove(position);
                    request_received_adapter.notifyDataSetChanged();

                    if(request_received_list.size() > 0) {
                        no_data_layout.setVisibility(View.GONE);
                                } else {
                        no_data_layout.setVisibility(View.VISIBLE);
                                }
                            } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), c.msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
