package com.smitiv.datify.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smitiv.datify.Models.Request_sent_User_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Sessions.Stored_datas;

import java.util.ArrayList;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Request_sent_Fragment extends Fragment {

    ArrayList<Request_sent_User_List> request_sent_list;
    RecyclerView sent_recyclerview;
    RecyclerView.Adapter request_sent_adapter;
    RelativeLayout no_data_layout;

    public Request_sent_Fragment() {
            }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_sent_layout, container, false);;

        init_view(view);
        return view;
    }

    private void init_view(View view) {
        sent_recyclerview = (RecyclerView) view.findViewById(R.id.sent_recyclerview);
        no_data_layout = (RelativeLayout) view.findViewById(R.id.no_data_layout);
        request_sent_list = new ArrayList<Request_sent_User_List>();
        request_sent_list = Stored_datas.getInstance().getRequest_sent_list();

        if(request_sent_list != null) {
            no_data_layout.setVisibility(View.GONE);
            if (request_sent_list.size() > 0) {
                sent_recyclerview.setHasFixedSize(true);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());    // LinearLayout for Listview
                sent_recyclerview.setLayoutManager(manager);
                request_sent_adapter = new Request_sent_Adapter(getActivity(), request_sent_list);
                sent_recyclerview.setAdapter(request_sent_adapter);
                request_sent_adapter.notifyDataSetChanged();
                no_data_layout.setVisibility(View.GONE);
                        } else {
                no_data_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
            no_data_layout.setVisibility(View.VISIBLE);
                    }
                }

    class Request_sent_Adapter extends RecyclerView.Adapter<Request_sent_Adapter.ViewHolder> {
        Context context;
        ArrayList<Request_sent_User_List> request_sent_list;

        public Request_sent_Adapter(Context context,  ArrayList<Request_sent_User_List> request_sent_list) {
            this.context = context;
            this.request_sent_list = request_sent_list;
        }
        @Override
        public Request_sent_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_sent_items, parent, false);
            return new Request_sent_Adapter.ViewHolder(view);
                    }

        @Override
        public void onBindViewHolder(Request_sent_Adapter.ViewHolder holder, final int position) {
            holder.r_sent_name.setText(request_sent_list.get(position).getFullname());

            if(request_sent_list.get(position).getJob_position().equalsIgnoreCase("")) {
                holder.r_sent_age.setText("Age: "+request_sent_list.get(position).getAge());
                        } else {
                holder.r_sent_age.setText(request_sent_list.get(position).getAge() + ", ");
                    }

            holder.r_sent_designation.setText(request_sent_list.get(position).getJob_position());
            Glide.with(context).load(request_sent_list.get(position).getProfilepicture()).into(holder.r_sent_user_image);
                    }

        @Override
        public int getItemCount() {
            return request_sent_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView r_sent_user_image;
            TextView r_sent_name;
            TextView r_sent_age, r_sent_designation;

            public ViewHolder(View itemView) {
                super(itemView);
                r_sent_name = (TextView)itemView.findViewById(R.id.r_sent_name);
                r_sent_age = (TextView)itemView.findViewById(R.id.r_sent_age);
                r_sent_designation = (TextView)itemView.findViewById(R.id.r_sent_designation);
                r_sent_user_image = (ImageView)itemView.findViewById(R.id.r_sent_user_image);
                    }
                }
            }

}
