package com.smitiv.datify.Dashboard_Classes;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Activities.Chat_List_Screen;
import com.smitiv.datify.Activities.Feedback_Screen;
import com.smitiv.datify.Activities.Filter_Screen;
import com.smitiv.datify.Activities.Login_Screen;
import com.smitiv.datify.Activities.My_Matches_Screen;
import com.smitiv.datify.Activities.My_Requests_Screen;
import com.smitiv.datify.Activities.New_His_Profile_Screen;
import com.smitiv.datify.Activities.New_My_Profile_Screen;
import com.smitiv.datify.Activities.Recent_Views;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Chat_Details_List;
import com.smitiv.datify.Models.Filtered_Matches_List;
import com.smitiv.datify.Models.My_Matches_List;
import com.smitiv.datify.Models.Profile_List;
import com.smitiv.datify.Models.Recent_Views_List;
import com.smitiv.datify.Models.Request_received_User_List;
import com.smitiv.datify.Models.Request_sent_User_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Chat_List_Response;
import com.smitiv.datify.Response.Common_Response;
import com.smitiv.datify.Response.Filtered_Matches_Response;
import com.smitiv.datify.Response.My_Matches_Response;
import com.smitiv.datify.Response.Profile_Response;
import com.smitiv.datify.Response.Recent_Views_Response;
import com.smitiv.datify.Response.Request_received_Response;
import com.smitiv.datify.Response.Request_sent_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , Intent_Constants, View.OnClickListener {

    NavigationView navigationView2;
    DrawerLayout drawer;
    ImageView menuRight;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;
    CircularImageView user_image;
    TextView user_name, user_email;

    public static MyAppAdapter myAppAdapter;
    public static ViewHolder viewHolder;
    ArrayList<Filtered_Matches_List> filtered_matches_lists;
    Filtered_Matches_List filtered_matches_lists_model;
    private SwipeFlingAdapterView flingContainer;
    ImageView delete;
    ImageView select;

    ImageView recent_view, my_request, my_matches_chat, my_profile;
    LinearLayout layout_profile, layout_my_matches, layout_msg_from_match, layout_msg_from_all,
            layout_filter, layout_feedback, layout_share, layout_logout;
    TextView version_name;

    Retrofit retrofit_obj;
    String mess;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ArrayList<Profile_List> profile_list;
    int swype_status = 0;

    ArrayList<Request_sent_User_List> request_sent_list;
    ArrayList<Request_received_User_List> request_received_list;
    Request_sent_User_List request_sent_list_model;
    Request_received_User_List request_received_list_model;
    ArrayList<My_Matches_List> my_matches_list;
    My_Matches_List my_matches_list_model;
    AlertDialog dialog;

    ArrayList<Recent_Views_List> recent_views_list;
    Recent_Views_List recent_views_list_model;

    ArrayList<Chat_Details_List> chat_details_list;
    Chat_Details_List chat_details_list_model;
    String swyped_name = "";
    String swyped_fb_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        navigation_init();
        drawer_init();
        card_swype_init();
           }

    private void card_swype_init() {
        delete = (ImageView) findViewById(R.id.delete);
        select = (ImageView) findViewById(R.id.select);

        recent_view = (ImageView) findViewById(R.id.recent_view);
        my_request = (ImageView) findViewById(R.id.my_request);
        my_matches_chat = (ImageView) findViewById(R.id.my_matches_chat);
        my_profile = (ImageView) findViewById(R.id.my_profile);

        recent_view.setOnClickListener(this);
        my_request.setOnClickListener(this);
        my_matches_chat.setOnClickListener(this);
        my_profile.setOnClickListener(this);

        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        filtered_matches_lists = new ArrayList<Filtered_Matches_List>();

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new get_filtered_profiles(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }

    private void card_swype_listeners() {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(delete, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                String to_fb_id = filtered_matches_lists.get(0).getUser_fb_id();
                flingContainer.getTopCardListener().selectLeft();

                Log.e(DATIFY_LOG, "Removed_Fb_Id: " + to_fb_id);
                send_like_data(str_user_fb_id, to_fb_id, "R");
                        }});

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(select, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                String to_fb_id = filtered_matches_lists.get(0).getUser_fb_id();
                flingContainer.getTopCardListener().selectRight();

                Log.e(DATIFY_LOG, "Liked_Fb_Id: " + to_fb_id);
                send_like_data(str_user_fb_id, to_fb_id, "S");
                        }});

//        flingContainer.set
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                swyped_name = filtered_matches_lists.get(0).getFullname();
                swyped_fb_id = filtered_matches_lists.get(0).getUser_fb_id();
                Log.e(DATIFY_LOG, "Remove_First_Object");
                Log.e(DATIFY_LOG, "Swyped_Name: " + swyped_name);
                filtered_matches_lists.remove(0);
                myAppAdapter.notifyDataSetChanged();
                if(filtered_matches_lists.size() == 0) {
                    delete.setVisibility(View.GONE);
                    select.setVisibility(View.GONE);
                            }
                        }

            @Override
            public void onLeftCardExit(Object dataObject) {
                Log.e(DATIFY_LOG, "Rejected");
                Log.e(DATIFY_LOG, "Rejected_Name: " + swyped_name);
                Log.e(DATIFY_LOG, "Rejected_fb_id: " + swyped_fb_id);
                send_swype_like_data(str_user_fb_id, swyped_fb_id, "R");
                            }

            @Override
            public void onRightCardExit(Object dataObject) {
                Log.e(DATIFY_LOG, "Liked");
                Log.e(DATIFY_LOG, "Selected_Name: " + swyped_name);
                Log.e(DATIFY_LOG, "Selected_fb_id: " + swyped_fb_id);
                send_swype_like_data(str_user_fb_id, swyped_fb_id, "S");
                        }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                        }
                    });

        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                      }

                String to_fb_id = filtered_matches_lists.get(itemPosition).getUser_fb_id();
                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new add_into_recent_view(str_user_fb_id, to_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                /*Toast.makeText(getApplicationContext(), filtered_matches_lists.get(itemPosition).getFullname() +
                        " Profile", Toast.LENGTH_SHORT).show();*/
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);

                myAppAdapter.notifyDataSetChanged();
                    } });
                }

    /*private void send_remove_data(String str_user_fb_id, String to_fb_id, String accept_status) {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
        }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new send_remove(str_user_fb_id, to_fb_id, accept_status).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }*/

    private void send_swype_like_data(String str_user_fb_id, String swyped_fb_id, String accept_status) {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new send_swyped_match(str_user_fb_id, swyped_fb_id, accept_status).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

    private void send_like_data(String str_user_fb_id, String to_fb_id, String accept_status) {

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        if(swype_status == 1) {
            mess = APIUrl.BASE_URL;
            retrofit_obj = new Retrofit.Builder()
                    .baseUrl(mess)
                    .addConverterFactory(GsonConverterFactory.create()).build();
            new send_match(str_user_fb_id, to_fb_id, accept_status).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

            }

    private class send_match extends AsyncTask<Void, Common_Response, Common_Response> {

        String my_fb_id, to_fb_id, accept_status;

        public send_match(String my_fb_id, String to_fb_id, String accept_status) {
            this.my_fb_id = my_fb_id;
            this.to_fb_id = to_fb_id;
            this.accept_status = accept_status;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Common_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Common_Response> call = a.sent_request(my_fb_id, to_fb_id, accept_status);
            Common_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Sent_Request_Response: " + c);
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
                }

        @Override
        protected void onPostExecute(Common_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    swype_status = 0;

                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                            } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }

    private void navigation_init() {
        navigationView2 = (NavigationView) findViewById(R.id.nav_view2);
        navigationView2.setNavigationItemSelectedListener(this);
        View header = ((NavigationView)findViewById(R.id.nav_view2)).getHeaderView(0);
        user_name = (TextView) header.findViewById(R.id.user_name);
        user_email = (TextView) header.findViewById(R.id.user_email);
        user_image = (CircularImageView) header.findViewById(R.id.user_image);

        layout_profile = (LinearLayout) header.findViewById(R.id.layout_profile);
        layout_my_matches = (LinearLayout) header.findViewById(R.id.layout_my_matches);
        layout_msg_from_match = (LinearLayout) header.findViewById(R.id.layout_msg_from_match);
        layout_msg_from_all = (LinearLayout) header.findViewById(R.id.layout_msg_from_all);
        layout_filter = (LinearLayout) header.findViewById(R.id.layout_filter);
        layout_feedback = (LinearLayout) header.findViewById(R.id.layout_feedback);
        layout_share = (LinearLayout) header.findViewById(R.id.layout_share);
        layout_logout = (LinearLayout) header.findViewById(R.id.layout_logout);
        layout_profile.setOnClickListener(this);
        layout_my_matches.setOnClickListener(this);
        layout_msg_from_match.setOnClickListener(this);
        layout_msg_from_all.setOnClickListener(this);
        layout_filter.setOnClickListener(this);
        layout_feedback.setOnClickListener(this);
        layout_share.setOnClickListener(this);
        layout_logout.setOnClickListener(this);

        profile_list = new ArrayList<Profile_List>();
        request_sent_list = new ArrayList<Request_sent_User_List>();
        request_received_list = new ArrayList<Request_received_User_List>();
        my_matches_list = new ArrayList<My_Matches_List>();
        recent_views_list = new ArrayList<Recent_Views_List>();
        chat_details_list = new ArrayList<Chat_Details_List>();

        dialog = new SpotsDialog(Dashboard.this);

        version_name = (TextView) header.findViewById(R.id.version_name);

        pref_validation();
              }

    private void drawer_init() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuRight = (ImageView) findViewById(R.id.menuRight);

        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    pref_validation();
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    pref_validation();
                    drawer.openDrawer(GravityCompat.START);
                        }
                    }
                });
            }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Dashboard.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id","");
        str_user_name = pref.getString("datify_name","");
        str_user_email = pref.getString("datify_email","");
        str_user_profile_img = pref.getString("datify_profile_img","");
        str_user_dob = pref.getString("datify_dob","");
        editor.commit();

        Log.e(DATIFY_LOG, "Fb_id: " + str_user_fb_id);

        user_name.setText(str_user_name);
        user_email.setText(str_user_email);
        Glide.with(getApplicationContext()).load(str_user_profile_img).into(user_image);

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            String version  = pInfo.versionName;
            version_name.setText("Version "+version);
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                    } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_profile:
                get_profile();
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.layout_my_matches:
                get_my_matches();
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.layout_msg_from_match:
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(layout_msg_from_match, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                    }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new msg_from_match(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.layout_msg_from_all:
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(layout_msg_from_all, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new msg_from_all(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.layout_filter:
                Intent filter_intent = new Intent(Dashboard.this, Filter_Screen.class);
                startActivityForResult(filter_intent, DASHBOARD_to_FILTER);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.layout_feedback:
                Intent feedback_intent = new Intent(Dashboard.this, Feedback_Screen.class);
                startActivityForResult(feedback_intent, DASHBOARD_to_FEEDBACK);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.layout_share:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: http://smitiv.co/tinder-clone-dating-app-development-singapore.php");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.layout_logout:
                verify_logout();
                break;
            case R.id.recent_view:
                get_recent_views();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.my_request:
                get_request_sent_users_list();

                break;
            case R.id.my_matches_chat:
                get_my_matches();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.my_profile:
                get_profile();
                drawer.closeDrawer(GravityCompat.START);
                break;
                    }
                }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    private void get_recent_views() {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(layout_profile, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
                return;
                    }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new get_recent_views(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

    private void get_my_matches() {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(layout_profile, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new get_my_matches_datas(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

    private void get_request_sent_users_list() {

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(layout_profile, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new get_request_sent_users(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

    private void verify_logout() {
        new android.support.v7.app.AlertDialog.Builder(Dashboard.this)
                .setTitle("Logout")
                .setMessage("Are you really want to leave Datify?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        editor.clear();
                        editor.apply();
                        String login_status = Stored_datas.getInstance().getLoginstatus();
                        if(login_status.equalsIgnoreCase("1")) {
                            setResult(LOGOUT);
                            finish();
                               } else {
                            Toast.makeText(getApplicationContext(),"Successfully Logged out", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Dashboard.this, Login_Screen.class);
                            startActivity(intent);
                            finish();
                                }
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
                }

    private void get_profile() {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(layout_profile, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new view_profile(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }

    private class view_profile extends AsyncTask<Void, Profile_Response,
            Profile_Response> {

//        AlertDialog dialog = new SpotsDialog(Dashboard.this);
        String fb_id;

        public view_profile(String fb_id) {
            this.fb_id = fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Profile_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Profile_Response> call = a.view_my_profile(fb_id);
            Profile_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "FB_Login_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Profile_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    profile_list.clear();
                    dialog.dismiss();

                    profile_list.add(new Profile_List(c.id, c.fb_id, c.fullname, c.email, c.profilepicture, c.gender,
                            c.dob, c.looking_for, c.distance, c.alerts, c.about_me, c.education, c.marital_status,
                            c.interest, c.location, c.job_position, c.age, c.weight, c.height, c.cover_photo_link));

                Log.e(DATIFY_LOG, "Profile_Name: " + c.fullname);
                Stored_datas.getInstance().setProfile_list(profile_list);
                Intent profile_intent = new Intent(Dashboard.this, New_My_Profile_Screen.class);
                startActivityForResult(profile_intent, DASHBOARD_to_PROFILE);

                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }

    public static class ViewHolder {
        public static FrameLayout background;
        public ImageView cardImage;
        public TextView name, designation, age, location;
        public LinearLayout details_layout;
                }

    public class MyAppAdapter extends BaseAdapter {
        ArrayList<Filtered_Matches_List> filtered_matches_lists;
        public Context context;

        private MyAppAdapter(ArrayList<Filtered_Matches_List> filtered_matches_lists, Context context) {
            this.filtered_matches_lists = filtered_matches_lists;
            this.context = context;
        }

        @Override
        public int getCount() {
            return filtered_matches_lists.size();
                }

        @Override
        public Object getItem(int position) {
            return position;
                }

        @Override
        public long getItemId(int position) {
            return position;
                }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;

            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.cardswype_item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);

                viewHolder.name = (TextView) rowView.findViewById(R.id.name);
                viewHolder.age = (TextView) rowView.findViewById(R.id.age);
                viewHolder.designation = (TextView) rowView.findViewById(R.id.designation);
                viewHolder.location = (TextView) rowView.findViewById(R.id.location);
                viewHolder.details_layout = (LinearLayout) rowView.findViewById(R.id.details_layout);
                viewHolder.details_layout.bringToFront();
                rowView.setTag(viewHolder);
                        } else {
                viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.details_layout.bringToFront();
                    }
            viewHolder.name.setText(filtered_matches_lists.get(position).getFullname());

            if(filtered_matches_lists.get(position).getJob_position().equalsIgnoreCase("")) {
                viewHolder.age.setText("Age: "+filtered_matches_lists.get(position).getAge());
                        } else {
                viewHolder.age.setText(filtered_matches_lists.get(position).getAge() + ", ");
                        }
            viewHolder.designation.setText(filtered_matches_lists.get(position).getJob_position());
            viewHolder.location.setText(filtered_matches_lists.get(position).getLocation());
            Glide.with(context).load(filtered_matches_lists.get(position).getProfilepicture()).into(viewHolder.cardImage);
            return rowView;
                }
            }

    private class get_filtered_profiles extends AsyncTask<Void, Filtered_Matches_Response,
            Filtered_Matches_Response> {

        String fb_id;

        public get_filtered_profiles(String fb_id) {
            this.fb_id = fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Filtered_Matches_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Filtered_Matches_Response> call = a.filtered_match(fb_id);
            Filtered_Matches_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Filtered_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Filtered_Matches_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    filtered_matches_lists.clear();
                    if (null != c.filtered_match_results && c.filtered_match_results.size() > 0) {
                        for (Filtered_Matches_Response.Filtered_Match_Result dd : c.filtered_match_results) {

                            filtered_matches_lists_model = new Filtered_Matches_List(dd.user_fb_id, dd.fullname, dd.email,
                                    dd.profilepicture, dd.gender, dd.dob,dd.looking_for, dd.distance,
                                    dd.alerts, dd.about_me, dd.education, dd.marital_status, dd.interest,
                                    dd.location, dd.job_position, dd.age,dd.weight, dd.height, dd.distance_in_km);
                            filtered_matches_lists.add(filtered_matches_lists_model);
                            Log.e(DATIFY_LOG, "Name: " + dd.fullname);
                            Log.e(DATIFY_LOG, "Image: " + dd.profilepicture);
                                                }
                                            }
                     dialog.dismiss();
                    myAppAdapter = new MyAppAdapter(filtered_matches_lists, Dashboard.this);
                    flingContainer.setAdapter(myAppAdapter);
                    myAppAdapter.notifyDataSetChanged();

                    card_swype_listeners();

                       } else if (c.status == 0) {
                          delete.setVisibility(View.GONE);
                          select.setVisibility(View.GONE);
                          dialog.dismiss();

                    AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
                    builder.setTitle("No profile found...");
                    builder.setMessage("Please update your profile and search your query on filter");
                    builder.setCancelable(false);

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                    dialog.show();
//                    Toast.makeText(Dashboard.this, "No profile found" , Toast.LENGTH_LONG).show();
//                       Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                           }
                        }}
                   }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return false;
            }

    private class get_request_sent_users extends AsyncTask<Void, Request_sent_Response,
            Request_sent_Response> {

        String fb_id;

        public get_request_sent_users(String fb_id) {
            this.fb_id = fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Request_sent_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Request_sent_Response> call = a.request_sent_api(fb_id);
            Request_sent_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Request_sent_resp: " + c.toString());
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
             }

        @Override
        protected void onPostExecute(Request_sent_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    request_sent_list.clear();
                    if (null != c.request_sent_result && c.request_sent_result.size() > 0) {
                        for (Request_sent_Response.Request_sent_Result dd : c.request_sent_result) {

                            request_sent_list_model = new Request_sent_User_List(dd.user_fb_id, dd.fullname, dd.email,
                                    dd.profilepicture, dd.gender, dd.dob, dd.job_position, dd.age);
                            request_sent_list.add(request_sent_list_model);
                                        }
                                    }
                    Stored_datas.getInstance().setRequest_sent_list(request_sent_list);
                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new get_received_users(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else if (c.status == 0) {
                    Stored_datas.getInstance().setRequest_sent_list(null);
                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new get_received_users(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    dialog.dismiss();
//                    Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                        }
                    }}
                }

    private class get_received_users extends AsyncTask<Void, Request_received_Response,
            Request_received_Response> {

        String fb_id;

        public get_received_users(String fb_id) {
            this.fb_id = fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                }

        @Override
        protected Request_received_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Request_received_Response> call = a.request_received_api(fb_id);
            Request_received_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Request_sent_resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Request_received_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    request_received_list.clear();
                    if (null != c.request_received_result && c.request_received_result.size() > 0) {
                        for (Request_received_Response.Request_received_Result dd : c.request_received_result) {

                            request_received_list_model = new Request_received_User_List(dd.from_fb_id, dd.fullname, dd.email,
                                    dd.profilepicture, dd.gender, dd.dob, dd.job_position, dd.age);
                            request_received_list.add(request_received_list_model);
                                    }
                                }
                    dialog.dismiss();
                    Stored_datas.getInstance().setRequest_received_list(request_received_list);

                    Intent my_request_intent = new Intent(Dashboard.this, My_Requests_Screen.class);
                    startActivityForResult(my_request_intent, DASHBOARD_to_MY_REQUEST);
                    drawer.closeDrawer(GravityCompat.START);

                } else if (c.status == 0) {
                    dialog.dismiss();
                    Stored_datas.getInstance().setRequest_received_list(null);

                    Intent my_request_intent = new Intent(Dashboard.this, My_Requests_Screen.class);
                    startActivityForResult(my_request_intent, DASHBOARD_to_MY_REQUEST);
                    drawer.closeDrawer(GravityCompat.START);
                }
            }}
    }

    private class get_my_matches_datas extends AsyncTask<Void, My_Matches_Response,
            My_Matches_Response> {

        String fb_id;

        public get_my_matches_datas(String fb_id) {
            this.fb_id = fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected My_Matches_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<My_Matches_Response> call = a.request_my_matches(fb_id);
            My_Matches_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"My_Matches_Resp: " + c.toString());
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
            }

        @Override
        protected void onPostExecute(My_Matches_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    my_matches_list.clear();
                    if (null != c.my_matches_result && c.my_matches_result.size() > 0) {
                        for (My_Matches_Response.My_Matches_Result dd : c.my_matches_result) {
                            my_matches_list_model = new My_Matches_List(dd.user_fb_id, dd.fullname, dd.email,
                                    dd.profilepicture, dd.gender, dd.dob,dd.looking_for, dd.distance,
                                    dd.alerts, dd.about_me, dd.education, dd.marital_status, dd.interest,
                                    dd.location, dd.job_position, dd.age,dd.weight, dd.height, dd.distance_in_km);
                            my_matches_list.add(my_matches_list_model);
                                }
                            }
                    dialog.dismiss();
                    Stored_datas.getInstance().setMy_matches_list(my_matches_list);
                    Intent my_matches_intent = new Intent(Dashboard.this, My_Matches_Screen.class);
                    startActivityForResult(my_matches_intent, DASHBOARD_to_MY_MATCHES);

                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                        }
                    }}
                }

    private class add_into_recent_view extends AsyncTask<Void, Common_Response, Common_Response> {

        String my_fb_id, to_fb_id;

        public add_into_recent_view(String my_fb_id, String to_fb_id) {
            this.my_fb_id = my_fb_id;
            this.to_fb_id = to_fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Common_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Common_Response> call = a.recent_views(my_fb_id, to_fb_id);
            Common_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Sent_Request_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Common_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    Log.e(DATIFY_LOG, "Response 1");

                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new view_his_profile(to_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private class view_his_profile extends AsyncTask<Void, Profile_Response,
            Profile_Response> {

        String his_fb_id;

        public view_his_profile(String his_fb_id) {
            this.his_fb_id = his_fb_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog.show();
                    }

        @Override
        protected Profile_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Profile_Response> call = a.view_my_profile(his_fb_id);
            Profile_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "FB_Login_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Profile_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    profile_list.clear();
                    dialog.dismiss();

                    profile_list.add(new Profile_List(c.id, c.fb_id, c.fullname, c.email, c.profilepicture, c.gender,
                            c.dob, c.looking_for, c.distance, c.alerts, c.about_me, c.education, c.marital_status,
                            c.interest, c.location, c.job_position, c.age, c.weight, c.height, c.cover_photo_link));

                    Log.e(DATIFY_LOG, "Profile_Name: " + c.fullname);
                    Stored_datas.getInstance().setProfile_list(profile_list);
                    Intent profile_intent = new Intent(Dashboard.this, New_His_Profile_Screen.class);
                    startActivityForResult(profile_intent, DASHBOARD_to_PROFILE);

                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private class send_swyped_match extends AsyncTask<Void, Common_Response, Common_Response> {

        String my_fb_id, to_fb_id, accept_status;

        public send_swyped_match(String my_fb_id, String to_fb_id, String accept_status) {
            this.my_fb_id = my_fb_id;
            this.to_fb_id = to_fb_id;
            this.accept_status = accept_status;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                    }

        @Override
        protected Common_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Common_Response> call = a.sent_request(my_fb_id, to_fb_id, accept_status);
            Common_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Sent_Request_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Common_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

    private class get_recent_views extends AsyncTask<Void, Recent_Views_Response,
            Recent_Views_Response> {

        String fb_id;

        public get_recent_views(String fb_id) {
            this.fb_id = fb_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Recent_Views_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Recent_Views_Response> call = a.request_recent_views(fb_id);
            Recent_Views_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"My_Matches_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Recent_Views_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    recent_views_list.clear();
                    if (null != c.recent_views_results && c.recent_views_results.size() > 0) {
                        for (Recent_Views_Response.Recent_Views_Result dd : c.recent_views_results) {
                            recent_views_list_model = new Recent_Views_List(dd.user_fb_id, dd.fullname, dd.email,
                                    dd.profilepicture, dd.job_position, dd.age, dd.viewed_date);
                            recent_views_list.add(recent_views_list_model);
                                    }
                                }
                    dialog.dismiss();
                    Stored_datas.getInstance().setRecent_views_list(recent_views_list);
                    Intent recent_views_intent = new Intent(Dashboard.this, Recent_Views.class);
                    startActivityForResult(recent_views_intent, DASHBOARD_to_RECENT_VIEWS);

                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                }
            }}
    }

    private class msg_from_match extends AsyncTask<Void, Chat_List_Response,
            Chat_List_Response> {

        String fb_id;

        public msg_from_match(String fb_id) {
            this.fb_id = fb_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Chat_List_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_List_Response> call = a.get_msg_from_matches(fb_id);
            Chat_List_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"My_Matches_Resp: " + c.toString());
                        } catch (IOException e) {
                e.printStackTrace();
                    }
                return c;
                }

        @Override
        protected void onPostExecute(Chat_List_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_details_list.clear();
                    if (null != c.chat_list_result && c.chat_list_result.size() > 0) {
                        for (Chat_List_Response.Chat_List_Result dd : c.chat_list_result) {
                            chat_details_list_model = new Chat_Details_List(dd.chat_user_id, dd.chat_user_name,
                                    dd.chat_user_photo, dd.chat_message, dd.chat_date, dd.chat_time, dd.chat_status);
                            chat_details_list.add(chat_details_list_model);
                                    }
                                }
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_details_list(chat_details_list);
                    Intent my_matches_intent = new Intent(Dashboard.this, Chat_List_Screen.class);
                    startActivityForResult(my_matches_intent, DASHBOARD_to_CHAT_LIST_SCREEN);
                            } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                            }
                        }}
                    }

    private class msg_from_all extends AsyncTask<Void, Chat_List_Response,
            Chat_List_Response> {

        String fb_id;

        public msg_from_all(String fb_id) {
            this.fb_id = fb_id;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Chat_List_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_List_Response> call = a.get_msg_from_all(fb_id);
            Chat_List_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"My_Matches_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Chat_List_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_details_list.clear();
                    if (null != c.chat_list_result && c.chat_list_result.size() > 0) {
                        for (Chat_List_Response.Chat_List_Result dd : c.chat_list_result) {
                            chat_details_list_model = new Chat_Details_List(dd.chat_user_id, dd.chat_user_name,
                                    dd.chat_user_photo, dd.chat_message, dd.chat_date, dd.chat_time, dd.chat_status);
                            chat_details_list.add(chat_details_list_model);
                                    }
                                }
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_details_list(chat_details_list);
                    Intent my_matches_intent = new Intent(Dashboard.this, Chat_List_Screen.class);
                    startActivityForResult(my_matches_intent, DASHBOARD_to_CHAT_LIST_SCREEN);
                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Dashboard.this, c.msg , Toast.LENGTH_LONG).show();
                        }
                    }}
                }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == DASHBOARD_to_FILTER) {
            if(resultCode == FILTER_updated) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(recent_view, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                       }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new get_filtered_profiles(str_user_fb_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                }
}
