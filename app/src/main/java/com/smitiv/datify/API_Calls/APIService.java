package com.smitiv.datify.API_Calls;

import com.smitiv.datify.Response.Chat_List_Response;
import com.smitiv.datify.Response.Chat_Receive_Response;
import com.smitiv.datify.Response.Chat_Send_Response;
import com.smitiv.datify.Response.Common_Response;
import com.smitiv.datify.Response.Cover_Photo_Response;
import com.smitiv.datify.Response.Edit_Profile_Response;
import com.smitiv.datify.Response.Feedback_Response;
import com.smitiv.datify.Response.Filter_Response;
import com.smitiv.datify.Response.Filtered_Matches_Response;
import com.smitiv.datify.Response.Image_Update_Response;
import com.smitiv.datify.Response.Login_Response;
import com.smitiv.datify.Response.My_Matches_Response;
import com.smitiv.datify.Response.Profile_Response;
import com.smitiv.datify.Response.Recent_Views_Response;
import com.smitiv.datify.Response.Request_received_Response;
import com.smitiv.datify.Response.Request_sent_Response;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @FormUrlEncoded
    @POST("login.php")
    Call<Login_Response> fb_login(@Field("fb_id") String fb_id, @Field("fullname") String fullname,
                                          @Field("email") String email, @Field("profilepicture") String profilepicture,
                                          @Field("dob") String dob, @Field("devicetoken") String devicetoken,
                                          @Field("device_type") String device_type, @Field("lat") String lat,
                                          @Field("lng") String lng, @Field("gender") String gender,
                                          @Field("cover_photo_link") String cover_photo_link);

    @FormUrlEncoded
    @POST("view_profile.php")
    Call<Profile_Response> view_my_profile(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("update_profile_photo.php")
    Call<Image_Update_Response> update_my_photo(@Field("fb_id") String fb_id, @Field("profile_photo") String profile_photo);

    @FormUrlEncoded
    @POST("send_feedback.php")
    Call<Feedback_Response> send_feedback(@Field("user_name") String user_name, @Field("email_id") String email_id,
                                     @Field("feedback") String feedback, @Field("rating") String rating,
                                     @Field("app_version") String app_version);

    @FormUrlEncoded
    @POST("user_search_filter.php")
    Call<Filter_Response> update_filter(@Field("fb_id") String fb_id, @Field("looking_for") String looking_for,
                                        @Field("distance") String distance, @Field("alerts") String alerts);

    @FormUrlEncoded
    @POST("get_filter_by_matches.php")
    Call<Filtered_Matches_Response> filtered_match(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("request_sent.php")
    Call<Common_Response> sent_request(@Field("from_fb_id") String from_fb_id, @Field("to_fb_id") String to_fb_id,
                                       @Field("accept_status") String accept_status);

    @FormUrlEncoded
    @POST("get_invited_users.php")
    Call<Request_received_Response> request_received_api(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("get_request_sent_users.php")
    Call<Request_sent_Response> request_sent_api(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("request_accept_reject.php")
    Call<Common_Response> request_results(@Field("from_fb_id") String from_fb_id, @Field("to_fb_id") String to_fb_id,
                                       @Field("accept_status") String accept_status);

    @FormUrlEncoded
    @POST("my_matches.php")
    Call<My_Matches_Response> request_my_matches(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("recent_view.php")
    Call<Common_Response> recent_views(@Field("from_fb_id") String from_fb_id, @Field("to_fb_id") String to_fb_id);

    @FormUrlEncoded
    @POST("recent_view_list.php")
    Call<Recent_Views_Response> request_recent_views(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("receice_message.php")
    Call<Chat_Receive_Response> get_receive_chat(@Field("from_fb_id") String from_fb_id, @Field("to_fb_id") String to_fb_id);

    @FormUrlEncoded
    @POST("send_message.php")
    Call<Chat_Send_Response> chat_send_msg(@Field("from_fb_id") String from_fb_id, @Field("to_fb_id") String to_fb_id,
                                           @Field("chat_message") String chat_message);

    @FormUrlEncoded
    @POST("chat_list.php")
    Call<Chat_List_Response> get_msg_from_all(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("chat_list_matches.php")
    Call<Chat_List_Response> get_msg_from_matches(@Field("fb_id") String fb_id);

    @FormUrlEncoded
    @POST("edit_profile.php")
    Call<Edit_Profile_Response> update_profile(@Field("fb_id") String fb_id, @Field("about_me") String about_me,
                                               @Field("education") String education, @Field("marital_status") String marital_status,
                                               @Field("interest") String interest, @Field("location") String location,
                                               @Field("job_position") String job_position, @Field("weight") String weight,
                                               @Field("height") String height, @Field("dob") String dob,
                                               @Field("gender") String gender);

    @FormUrlEncoded
    @POST("update_cover_photo.php")
    Call<Cover_Photo_Response> update_cover_photo(@Field("fb_id") String fb_id, @Field("cover_photo") String cover_photo);
    /*@GET("search_tour_name.php")
    Call<Tour_Names_Response> get_Tour_Names();

    @GET("get_countries.php")
    Call<Country_Response> get_countries();*/

        }
