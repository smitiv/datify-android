package com.smitiv.datify.Sessions;


import com.smitiv.datify.Models.Chat_Details_List;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Models.My_Matches_List;
import com.smitiv.datify.Models.Profile_List;
import com.smitiv.datify.Models.Recent_Views_List;
import com.smitiv.datify.Models.Request_received_User_List;
import com.smitiv.datify.Models.Request_sent_User_List;

import java.util.ArrayList;

public class Stored_datas {

    private static Stored_datas myObj;
    ArrayList<Profile_List> profile_list;
    String loginstatus;
    ArrayList<Request_sent_User_List> request_sent_list;
    ArrayList<Request_received_User_List> request_received_list;
    ArrayList<My_Matches_List> my_matches_list;
    ArrayList<Recent_Views_List> recent_views_list;
    ArrayList<Chat_Receive_List> chat_receive_list;
    ArrayList<Chat_Details_List> chat_details_list;

    String chat_his_fb_id;
    String chat_his_name;
    String chat_his_image;

    String prof_profile_picture;
    String prof_cover_photo;
    String prof_designation;
    String prof_about_me;
    String prof_weight;
    String prof_height;
    String prof_marital_status;
    String chat_list_position;
    String chat_from_status;

    String reg_name;
    String reg_fb_id;
    String reg_email;
    String reg_profile_img;
    String reg_device_token;
    String reg_device_type;
    String reg_latitude;
    String reg_longitude;

    public String getReg_name() {
        return reg_name;
            }
    public void setReg_name(String reg_name) {
        this.reg_name = reg_name;
            }

    public String getReg_fb_id() {
        return reg_fb_id;
            }
    public void setReg_fb_id(String reg_fb_id) {
        this.reg_fb_id = reg_fb_id;
            }

    public String getReg_email() {
        return reg_email;
            }
    public void setReg_email(String reg_email) {
        this.reg_email = reg_email;
            }

    public String getReg_profile_img() {
        return reg_profile_img;
            }
    public void setReg_profile_img(String reg_profile_img) {
        this.reg_profile_img = reg_profile_img;
            }

    public String getReg_device_token() {
        return reg_device_token;
            }
    public void setReg_device_token(String reg_device_token) {
        this.reg_device_token = reg_device_token;
            }

    public String getReg_device_type() {
        return reg_device_type;
            }
    public void setReg_device_type(String reg_device_type) {
        this.reg_device_type = reg_device_type;
            }

    public String getReg_latitude() {
        return reg_latitude;
            }
    public void setReg_latitude(String reg_latitude) {
        this.reg_latitude = reg_latitude;
            }

    public String getReg_longitude() {
        return reg_longitude;
            }
    public void setReg_longitude(String reg_longitude) {
        this.reg_longitude = reg_longitude;
            }

    public String getChat_from_status() {
        return chat_from_status;
            }
    public void setChat_from_status(String chat_from_status) {
        this.chat_from_status = chat_from_status;
            }

    public String getChat_list_position() {
        return chat_list_position;
                }
    public void setChat_list_position(String chat_list_position) {
        this.chat_list_position = chat_list_position;
                }

    public String getProf_profile_picture() {
        return prof_profile_picture;
                }
    public void setProf_profile_picture(String prof_profile_picture) {
        this.prof_profile_picture = prof_profile_picture;
                }

    public String getProf_cover_photo() {
        return prof_cover_photo;
                }
    public void setProf_cover_photo(String prof_cover_photo) {
        this.prof_cover_photo = prof_cover_photo;
                }

    public String getProf_designation() {
        return prof_designation;
                }
    public void setProf_designation(String prof_designation) {
        this.prof_designation = prof_designation;
                }

    public String getProf_about_me() {
        return prof_about_me;
                }
    public void setProf_about_me(String prof_about_me) {
        this.prof_about_me = prof_about_me;
                }

    public String getProf_weight() {
        return prof_weight;
                }
    public void setProf_weight(String prof_weight) {
        this.prof_weight = prof_weight;
                }

    public String getProf_height() {
        return prof_height;
                }
    public void setProf_height(String prof_height) {
        this.prof_height = prof_height;
                }

    public String getProf_marital_status() {
        return prof_marital_status;
                }
    public void setProf_marital_status(String prof_marital_status) {
        this.prof_marital_status = prof_marital_status;
                }

    public ArrayList<Chat_Details_List> getChat_details_list() {
        return chat_details_list;
                }
    public void setChat_details_list(ArrayList<Chat_Details_List> chat_details_list) {
        this.chat_details_list = chat_details_list;
                }

    public String getChat_his_fb_id() {
        return chat_his_fb_id;
            }
    public void setChat_his_fb_id(String chat_his_fb_id) {
        this.chat_his_fb_id = chat_his_fb_id;
            }

    public String getChat_his_name() {
        return chat_his_name;
            }
    public void setChat_his_name(String chat_his_name) {
        this.chat_his_name = chat_his_name;
            }

    public String getChat_his_image() {
        return chat_his_image;
            }
    public void setChat_his_image(String chat_his_image) {
        this.chat_his_image = chat_his_image;
            }

    public ArrayList<Chat_Receive_List> getChat_receive_list() {
        return chat_receive_list;
                }
    public void setChat_receive_list(ArrayList<Chat_Receive_List> chat_receive_list) {
        this.chat_receive_list = chat_receive_list;
                }

    public ArrayList<Recent_Views_List> getRecent_views_list() {
        return recent_views_list;
            }
    public void setRecent_views_list(ArrayList<Recent_Views_List> recent_views_list) {
        this.recent_views_list = recent_views_list;
            }

    public ArrayList<My_Matches_List> getMy_matches_list() {
        return my_matches_list;
                }
    public void setMy_matches_list(ArrayList<My_Matches_List> my_matches_list) {
        this.my_matches_list = my_matches_list;
                }

    public ArrayList<Request_received_User_List> getRequest_received_list() {
        return request_received_list;
                }
    public void setRequest_received_list(ArrayList<Request_received_User_List> request_received_list) {
        this.request_received_list = request_received_list;
                }

    public ArrayList<Request_sent_User_List> getRequest_sent_list() {
        return request_sent_list;
                }
    public void setRequest_sent_list(ArrayList<Request_sent_User_List> request_sent_list) {
        this.request_sent_list = request_sent_list;
                }

    public ArrayList<Profile_List> getProfile_list() {
        return profile_list;
            }
    public void setProfile_list(ArrayList<Profile_List> profile_list) {
        this.profile_list = profile_list;
            }

    public String getLoginstatus() {
        return loginstatus;
    }
    public void setLoginstatus(String loginstatus) {
        this.loginstatus = loginstatus;
            }

    public static Stored_datas getMyObj() {
        return myObj;
            }
    public static void setMyObj(Stored_datas myObj) {
        Stored_datas.myObj = myObj;
    }
    public static Stored_datas getInstance(){
        if(myObj == null){
            myObj = new Stored_datas();
                }
        return myObj;
                }

            }
