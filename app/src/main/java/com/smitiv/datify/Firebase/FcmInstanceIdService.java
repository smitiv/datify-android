package com.smitiv.datify.Firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.R;

/**
 * Created by govindaraj on 17/04/18.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService implements Intent_Constants
{

// private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
// broadcast receiver intent filters
public static final String REGISTRATION_COMPLETE = "registrationComplete";
    private static final String TAG = "DATIFY_LOG";
    @Override
    public void onTokenRefresh() {
        Log.i(TAG,"FirebaseInstanceIdService");
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG,"FireBaseToken"+refreshedToken);
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
                }

        private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
                }

    private void storeRegIdInPref(String token) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editors = sharedPreferences.edit();
        editors.putString(getString(R.string.FCM_TOKEN), token);
        editors.commit();
                }
            }
