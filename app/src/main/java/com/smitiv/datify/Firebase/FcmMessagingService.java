package com.smitiv.datify.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smitiv.datify.Activities.Splash_Screen;
import com.smitiv.datify.R;

/**
 * Created by govindaraj on 17/04/18.
 */

public class FcmMessagingService extends FirebaseMessagingService {
    private static final String TAG = "DATIFY_LOG";
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG,"Inside From");
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG,"Inside From1");
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            //   handleNotification(remoteMessage.getNotification().getBody());
                        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG,"Inside From2");
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                    Log.e(TAG,"Reterived from(AdminPAnel code) for Tour Create Messages");
                    String title = remoteMessage.getData().get("title");
                    String message = remoteMessage.getData().get("message");
                    String flag = remoteMessage.getData().get("flag");

                   /* Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    resultIntent.putExtra("message", message);
                    showNotificationMessage(getApplicationContext(), title, message, resultIntent);*/
                    if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        // app is in foreground, broadcast the push message
                        Log.e(TAG,"App is in Foreground"+flag);
                            Intent resultIntent = new Intent(getApplicationContext(), Splash_Screen.class);
                            resultIntent.putExtra("message", message);
                            showNotificationMessage(getApplicationContext(), title, message, resultIntent);
                    } else {
                        // app is in background, show the notification in notification tray
                        Log.e(TAG,"App is in background");
                /*Session.getInstance().setNotificationstatus(2);*/
                        Intent resultIntent = new Intent(getApplicationContext(), Splash_Screen.class);
                        resultIntent.putExtra("message", message);
                        // check for image attachment
                        // image is present, show notification with image
                        showNotificationMessage(getApplicationContext(), title, message, resultIntent);
                    }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }

    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
        Log.e(TAG, "Notification_Showing");

        Intent intent1 = new Intent(this, Splash_Screen.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        notificationBuilder.setSmallIcon(R.drawable.datify_play_store_logo);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setColor(getResources().getColor(R.color.bottom_toolbar));
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
            }
        }
