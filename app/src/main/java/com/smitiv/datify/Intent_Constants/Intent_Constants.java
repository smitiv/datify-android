package com.smitiv.datify.Intent_Constants;


public interface Intent_Constants {

    public String DATIFY_LOG = "datify_log";
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    int LOGOUT = -1;

    int SPLASH_to_LOGIN = 1;
    int LOGIN_to_DASHBOARD = 2;
    int DASHBOARD_to_PROFILE = 3;
    int PROFILE_back = 4;
    int DASHBOARD_to_FEEDBACK = 5;
    int FEEDBACK_back = 6;
    int DASHBOARD_to_FILTER = 7;
    int FILTER_back = 8;
    int DASHBOARD_to_MY_REQUEST = 9;
    int MY_REQUEST_back = 10;
    int DASHBOARD_to_MY_MATCHES = 11;
    int DASHBOARD_to_RECENT_VIEWS = 12;
    int RECENT_VIEW_back = 13;
    int CHAT_back = 14;
    int HIS_PROFILE_to_CHAT = 15;
    int DASHBOARD_to_CHAT_LIST_SCREEN = 16;
    int MY_PROFILE_to_EDIT_PROFILE = 17;
    int PROFILE_UPDATED = 18;
    int EDIT_PROFILE_back = 19;
    int CHAT_LIST_to_CHAT = 20;
    int CHAT_LIST_back = 21;
    int MY_MATCHES_back = 22;
    int NEW_HIS_PROFILE_back = 23;
    int NEW_MY_PROFILE_back = 24;
    int FILTER_updated = 25;
    int INTERNET_CONNECTION = 26;
    int LOGIN_to_REGISTER_TWO = 27;

            }
