package com.smitiv.datify.Activities;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Profile_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Cover_Photo_Response;
import com.smitiv.datify.Response.Edit_Profile_Response;
import com.smitiv.datify.Response.Image_Update_Response;
import com.smitiv.datify.Sessions.Stored_datas;
import com.smitiv.datify.Support_Classes.PathUtil;
import com.smitiv.datify.Support_Classes.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
/*import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;*/

import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;


import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Edit_Profile_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener,
        DatePickerDialog.OnDateSetListener {
//,    DatePickerDialog.OnDateSetListener
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;

    ImageView back_arrow;
    private Toolbar toolbar;

    ImageView edit_cover_photo, update_cover_photo;
    CircleImageView my_profile_picture;
    ImageView update_profile_picture;
    EditText edit_designation;
    EditText about_me_text, edit_weight, height_feet, height_inch;
    MaterialSpinner marital_spinner, gender_spinner;
    TextView dob_link;
    LinearLayout save_profile_link;

    String str_cover_photo = "", str_designation = "", str_about_me = "",
            str_weight = "", str_height = "", str_marital_status = "";
    private ArrayAdapter<String> adapter;
    private static final String[] status_array = {"Single", "Married"};
    int photo_status = 0;
    private static final String[] gender_array = {"Male", "Female"};
    private ArrayAdapter<String> gender_adapter;
    String str_gender;
    DatePickerDialog check_in_dpd;
    TextView profi_name, profi_age;
    ArrayList<Profile_List> profile_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Edit_Profile_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                        }
                    });
                }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Edit_Profile_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_cover_photo = pref.getString("datify_cover_photo", "");
        str_user_dob = pref.getString("datify_dob", "");
        str_gender = pref.getString("datify_gender", "");
        editor.commit();

        str_about_me = Stored_datas.getInstance().getProf_about_me();
        str_designation = Stored_datas.getInstance().getProf_designation();
        str_height = Stored_datas.getInstance().getProf_height();
        str_weight = Stored_datas.getInstance().getProf_weight();
        str_marital_status = Stored_datas.getInstance().getProf_marital_status();
                }

    private void init_views() {
        profi_name = (TextView) findViewById(R.id.profi_name);
        profi_age = (TextView) findViewById(R.id.profi_age);
        profile_list = new ArrayList<Profile_List>();
        profile_list = Stored_datas.getInstance().getProfile_list();
        String str_name = "", str_age = "";

        if(profile_list.size()> 0) {
            str_name = profile_list.get(0).getFullname();
            str_age = profile_list.get(0).getAge();
                }

        if(!str_age.equalsIgnoreCase("")) {
            profi_age.setText(str_age);
                    } else {
            profi_age.setText("");
                }

        if(!str_name.equalsIgnoreCase("")) {
            if(str_age.equalsIgnoreCase("")) {
                profi_name.setText(str_name);
                        } else {
                profi_name.setText(str_name + ", ");
                    }
                }

        dob_link = (TextView) findViewById(R.id.dob_link);
        dob_link.setOnClickListener(this);

        if(str_user_dob != null) {
            if(!str_user_dob.equalsIgnoreCase("")) {
                dob_link.setText(str_user_dob);
                        } else {
                dob_link.setHint("Pick your Date-of-birth");
                        }
                }

        gender_spinner = (MaterialSpinner) findViewById(R.id.gender_spinner);
        edit_cover_photo = (ImageView) findViewById(R.id.edit_cover_photo);
        update_cover_photo = (ImageView) findViewById(R.id.update_cover_photo);
        update_profile_picture = (ImageView) findViewById(R.id.update_profile_picture);
        my_profile_picture = (CircleImageView) findViewById(R.id.my_profile_picture);
        edit_designation = (EditText) findViewById(R.id.edit_designation);
        about_me_text = (EditText) findViewById(R.id.about_me_text);
        edit_weight = (EditText) findViewById(R.id.edit_weight);
        height_feet = (EditText) findViewById(R.id.height_feet);
        height_inch = (EditText) findViewById(R.id.height_inch);

        marital_spinner = (MaterialSpinner) findViewById(R.id.marital_spinner);
        save_profile_link =(LinearLayout) findViewById(R.id.save_profile_link);
        save_profile_link.setOnClickListener(this);
        update_cover_photo.setOnClickListener(this);
        update_profile_picture.setOnClickListener(this);

        edit_designation.setText(str_designation);
        about_me_text.setText(str_about_me);
        edit_weight.setText(str_weight);

        if(!str_height.equalsIgnoreCase("")) {
            String[] parts = str_height.split("'");
            String s_feet = parts[0];
            String s_inch = parts[1];
            height_feet.setText(s_feet);
            height_inch.setText(s_inch);
                    }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, status_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_func();

        gender_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, gender_array);
        gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender_spinner_func();

        if(str_user_profile_img != null) {
            if (!str_user_profile_img.equalsIgnoreCase("")) {
                Glide.with(getApplicationContext()).load(str_user_profile_img).into(my_profile_picture);
                       }
                    }

        if(str_cover_photo != null) {
            if (!str_cover_photo.equalsIgnoreCase("")) {
                Glide.with(getApplicationContext()).load(str_cover_photo).into(edit_cover_photo);
                       }
                    }
                }

    private void gender_spinner_func() {
        gender_spinner.setAdapter(gender_adapter);
        gender_spinner.setPaddingSafe(0, 0, 0, 0);
        if(str_gender != null) {
            if (str_gender.equalsIgnoreCase("Male")) {
                gender_spinner.setSelection(1);
                    } else if (str_gender.equalsIgnoreCase("Female")) {
                gender_spinner.setSelection(2);
                    }
                 }

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_gender = parent.getItemAtPosition(position).toString();
                Log.e(DATIFY_LOG, "Gender_Data: " + str_gender);
                if(str_gender.equalsIgnoreCase("Gender")) {
                    str_gender = "";
                        }
                    }
            public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }

    private void spinner_func() {
        marital_spinner.setAdapter(adapter);
        marital_spinner.setPaddingSafe(0, 0, 0, 0);
        if(str_marital_status.equalsIgnoreCase("Single")) {
            marital_spinner.setSelection(1);
                    } else if(str_marital_status.equalsIgnoreCase("Married")) {
            marital_spinner.setSelection(2);
                }

        marital_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_marital_status = parent.getItemAtPosition(position).toString();
                Log.e(DATIFY_LOG, "Spinner_Data: " + str_marital_status);
                if(str_marital_status.equalsIgnoreCase("Marital status")) {
                    str_marital_status = "";
                        }
                    }
            public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

    @Override
    public void onBackPressed() {
        setResult(EDIT_PROFILE_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.dob_link:
                Calendar now = Calendar.getInstance();
                check_in_dpd = DatePickerDialog.newInstance(
                        Edit_Profile_Screen.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));

                check_in_dpd.setThemeDark(true); //set dark them for dialog?
                check_in_dpd.vibrate(false); //vibrate on choosing date?
                check_in_dpd.dismissOnPause(true); //dismiss dialog when onPause() called?
                check_in_dpd.showYearPickerFirst(false); //choose year first?
                check_in_dpd.setAccentColor(Color.parseColor("#9C27A0")); // custom accent color
                check_in_dpd.setTitle("Please select a date"); //dialog title
                Calendar max_calendar = Calendar.getInstance();
                max_calendar.set(1997, 05,01);
                check_in_dpd.setMaxDate(max_calendar);
                check_in_dpd.show(getFragmentManager(), "Date-of-Birth...");

                break;
            case R.id.save_profile_link:
                str_designation = "";
                str_about_me = "";
                str_weight = "";
                str_height = "";

                if(str_gender.equalsIgnoreCase("")) {
                    Snackbar.make(save_profile_link, "Please update your gender", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                      }

                if(str_user_dob.equalsIgnoreCase("")) {
                    Snackbar.make(save_profile_link, "Please update your DOB", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                str_designation = edit_designation.getText().toString().trim();
                str_about_me = about_me_text.getText().toString().trim();
                str_weight = edit_weight.getText().toString().trim();

                String str_hgt_feet = height_feet.getText().toString().trim();
                String str_hgt_inch = height_inch.getText().toString().trim();

                if(!str_hgt_feet.equalsIgnoreCase("") ) {
                        if(!str_hgt_inch.equalsIgnoreCase("")) {
                            str_height = str_hgt_feet.concat("'").concat(str_hgt_inch);
                                    } else {
                            str_height = str_hgt_feet.concat("'").concat("0");
                                    }
                                } else {
                        str_height = "";
                            }

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(update_profile_picture, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new update_profile(str_user_fb_id, str_designation, str_about_me, str_weight, str_height,
                        str_marital_status, str_gender, str_user_dob).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.update_cover_photo:
                photo_status = 1;
                CropImage.activity(null)
                        .setMinCropResultSize(500,200)
                        .setMaxCropResultSize(1000,600)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
//                upload_cover_photo();
                break;
            case R.id.update_profile_picture:
                photo_status = 2;
                CropImage.activity(null)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setBorderLineColor(getResources().getColor(R.color.bottom_toolbar))
                        .setGuidelinesColor(getResources().getColor(R.color.shadow_color))
                        .start(this);
//                upload_profile_photo();
                break;
                }
            }

    private void imagevalue(Bitmap bitmap) {
        String base64image_new = getStringImage(bitmap);
        Log.e(DATIFY_LOG, "Old_Base64: " + base64image_new);

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(update_profile_picture, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
                }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new update_my_profile_photo(str_user_fb_id, base64image_new).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

    private void cover_imagevalue(Bitmap bitmap) {
        String base64image_new = getStringImage(bitmap);
        Log.e(DATIFY_LOG, "Cover_Base64: " + base64image_new);
        /*base64CoverImg = resizeBase64Image(base64image_new);
        Log.e(DATIFY_LOG, "Cover_New_Base64: " + base64CoverImg);*/

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            Snackbar.make(update_profile_picture, "No Internet Connection", Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            return;
               }

        mess = APIUrl.BASE_URL;
        retrofit_obj = new Retrofit.Builder()
                .baseUrl(mess)
                .addConverterFactory(GsonConverterFactory.create()).build();
        new update_my_cover_photo(str_user_fb_id, base64image_new).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

    private String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.e("encodedImage", encodedImage);
        return encodedImage;
                }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(photo_status == 1) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                            CropImage.ActivityResult result = CropImage.getActivityResult(data);
                            if (resultCode == RESULT_OK) {
                                Uri uri = result.getUri();
                                if(uri != null) {
                                    try {
                                        String filePath= PathUtil.getPath(getApplicationContext(), uri);
                                        Log.e("image_path" , "Path: " + filePath);
                                        Bitmap bm = BitmapFactory.decodeFile(filePath);
                                        cover_imagevalue(bm);
                                        photo_status = 0;
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                                Toast.makeText(getApplicationContext(), "Some issues occured, please wait few minutes", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else if(photo_status == 2) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri uri = result.getUri();
                    if(uri != null) {
                        try {
                            String filePath= PathUtil.getPath(getApplicationContext(), uri);
                            Log.e("image_path" , "Path: " + filePath);
                            Bitmap bm = BitmapFactory.decodeFile(filePath);
//                            my_profile_picture.setImageURI(result.getUri());
                            imagevalue(bm);
                            photo_status = 0;
                                } catch (URISyntaxException e) {
                            e.printStackTrace();
                                }
                            }
                        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                            Toast.makeText(getApplicationContext(), "Some issues occured, please wait few minutes", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        str_user_dob =  year + "-" + (++monthOfYear) + "-" +  dayOfMonth;
        dob_link.setText(str_user_dob);
                }

    private class update_my_profile_photo extends AsyncTask<Void, Image_Update_Response,
            Image_Update_Response> {

        AlertDialog dialog = new SpotsDialog(Edit_Profile_Screen.this);
        String fb_id, base64String;

        public update_my_profile_photo(String fb_id, String base64String) {
            this.fb_id = fb_id;
            this.base64String = base64String;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Image_Update_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Image_Update_Response> call = a.update_my_photo(fb_id, base64String);
            Image_Update_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "FB_Login_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Image_Update_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();

                    editor = pref.edit();
                    editor.putString("datify_profile_img", c.profilepicture);
                    editor.commit();

                    Log.e(DATIFY_LOG, "Image_Url: " + c.profilepicture);

                    Glide.with(getApplicationContext()).load(c.profilepicture).into(my_profile_picture);
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                            } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Edit_Profile_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

    private class update_profile extends AsyncTask<Void, Edit_Profile_Response,
            Edit_Profile_Response> {

        AlertDialog dialog = new SpotsDialog(Edit_Profile_Screen.this);
        String fb_id, str_designation, str_about_me, str_weight, str_height,
                str_marital_status, str_gender, str_user_dob;

        public update_profile(String fb_id, String str_designation, String str_about_me, String str_weight,
                              String str_height, String str_marital_status, String str_gender, String str_user_dob) {
            this.fb_id = fb_id;
            this.str_designation = str_designation;
            this.str_about_me = str_about_me;
            this.str_weight = str_weight;
            this.str_height = str_height;
            this.str_marital_status = str_marital_status;
            this.str_gender = str_gender;
            this.str_user_dob = str_user_dob;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Edit_Profile_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Edit_Profile_Response> call = a.update_profile(fb_id, str_about_me, "", str_marital_status,
                    "", "", str_designation, str_weight, str_height, str_user_dob, str_gender);
            Edit_Profile_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Update_Profile_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Edit_Profile_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();
                    editor = pref.edit();
                    editor.putString("datify_dob", str_user_dob);
                    editor.putString("datify_gender", str_gender);
                    editor.commit();
                    Stored_datas.getInstance().setProf_about_me(c.about_me);
                    Stored_datas.getInstance().setProf_designation(c.job_position);
                    Stored_datas.getInstance().setProf_height(c.height);
                    Stored_datas.getInstance().setProf_weight(c.weight);
                    Stored_datas.getInstance().setProf_marital_status(c.marital_status);

                    setResult(PROFILE_UPDATED);
                    finish();
                         } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Edit_Profile_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

    private class update_my_cover_photo extends AsyncTask<Void, Cover_Photo_Response,
            Cover_Photo_Response> {

        AlertDialog dialog = new SpotsDialog(Edit_Profile_Screen.this);
        String fb_id, base64String;

        public update_my_cover_photo(String fb_id, String base64String) {
            this.fb_id = fb_id;
            this.base64String = base64String;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Cover_Photo_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Cover_Photo_Response> call = a.update_cover_photo(fb_id, base64String);
            Cover_Photo_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "FB_Login_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Cover_Photo_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();

                    Log.e(DATIFY_LOG, "Cover_Photo: " + c.cover_photo_link);
                    editor = pref.edit();
                    editor.putString("datify_cover_photo", c.cover_photo_link);
                    editor.commit();
                    Glide.with(getApplicationContext()).load(c.cover_photo_link).into(edit_cover_photo);
                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                            } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Edit_Profile_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
 }
