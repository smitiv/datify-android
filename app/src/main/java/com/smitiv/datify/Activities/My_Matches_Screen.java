package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.materialrangebar.RangeBar;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Fragments.Request_received_Fragment;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Models.My_Matches_List;
import com.smitiv.datify.Models.Request_received_User_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Chat_Receive_Response;
import com.smitiv.datify.Response.Filter_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class My_Matches_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;

    ImageView back_arrow;
    private Toolbar toolbar;
    RecyclerView my_matches_recyclerview;
    ArrayList<My_Matches_List> my_matches_list;
    RelativeLayout no_data_layout;
    RecyclerView.Adapter my_matches_adapter;

    AlertDialog dialog;
    ArrayList<Chat_Receive_List> chat_receive_list;
    Chat_Receive_List chat_receive_list_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_matches_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(My_Matches_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                   }
                });
            }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(My_Matches_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();
                }

    private void init_views() {
        dialog = new SpotsDialog(My_Matches_Screen.this);
        my_matches_recyclerview = (RecyclerView) findViewById(R.id.my_matches_recyclerview);
        my_matches_list = new ArrayList<My_Matches_List>();
        chat_receive_list = new ArrayList<Chat_Receive_List>();
        my_matches_list = Stored_datas.getInstance().getMy_matches_list();
        no_data_layout = (RelativeLayout) findViewById(R.id.no_data_layout);
        if(my_matches_list != null) {
            no_data_layout.setVisibility(View.GONE);
            if (my_matches_list.size() > 0) {
                my_matches_recyclerview.setHasFixedSize(true);
                my_matches_recyclerview.setLayoutManager(new GridLayoutManager(My_Matches_Screen.this, 2));
                my_matches_adapter = new My_Matches_Adapter(getApplicationContext(), my_matches_list);
                my_matches_recyclerview.setAdapter(my_matches_adapter);
                my_matches_adapter.notifyDataSetChanged();
                no_data_layout.setVisibility(View.GONE);
                    } else {
                no_data_layout.setVisibility(View.VISIBLE);
                    }
                } else {
            no_data_layout.setVisibility(View.VISIBLE);
                    }
                }

    @Override
    public void onBackPressed() {
        setResult(MY_MATCHES_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {

                }
            }

    private void show_message(String str) {
    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                }

    class My_Matches_Adapter extends RecyclerView.Adapter<My_Matches_Adapter.ViewHolder> {
        Context context;
        ArrayList<My_Matches_List> my_matches_list;

        public My_Matches_Adapter(Context context, ArrayList<My_Matches_List> my_matches_list) {
            this.context = context;
            this.my_matches_list = my_matches_list;
                }

        @Override
        public My_Matches_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_matches_item, parent, false);
            return new My_Matches_Adapter.ViewHolder(view);
                }

        @Override
        public void onBindViewHolder(My_Matches_Adapter.ViewHolder holder, final int position) {
            holder.matched_user_name.setText(my_matches_list.get(position).getFullname());
            holder.matched_user_age.setText(my_matches_list.get(position).getAge() + ", ");
            holder.matched_user_km.setText("Near by " + my_matches_list.get(position).getDistance_in_km()+ "km");
            Glide.with(context).load(my_matches_list.get(position).getProfilepicture()).into(holder.matched_user_image);
            holder.matched_user_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(my_matches_list.get(position).getProfilepicture() != null) {
                        if(!my_matches_list.get(position).getProfilepicture().equalsIgnoreCase("")) {
                            new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, v,
                                    my_matches_list.get(position).getProfilepicture(), null);
                                    }
                                }
                            }
                        });
            holder.chat_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String to_fb_id = my_matches_list.get(position).getUser_fb_id();

                    cd = new ConnectionDetector(getApplicationContext());
                    isInternetPresent = cd.isConnectingToInternet();
                    if (!isInternetPresent) {
                        Snackbar.make(view, "No Internet Connection", Snackbar.LENGTH_LONG).
                                setAction("Action", null).show();
                        return;
                            }

                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new get_chat_receive_list(str_user_fb_id, to_fb_id, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                   }
                             });
                        }

        @Override
        public int getItemCount() {
            return my_matches_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CircularImageView matched_user_image, chat_link;
            TextView matched_user_name, matched_user_age, matched_user_km;

            public ViewHolder(View itemView) {
                super(itemView);
                matched_user_image = (CircularImageView)itemView.findViewById(R.id.matched_user_image);
                chat_link = (CircularImageView)itemView.findViewById(R.id.chat_link);
                matched_user_name = (TextView)itemView.findViewById(R.id.matched_user_name);
                matched_user_age = (TextView)itemView.findViewById(R.id.matched_user_age);
                matched_user_km = (TextView)itemView.findViewById(R.id.matched_user_km);
                    }
                }
            }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
                    }

    private class get_chat_receive_list extends AsyncTask<Void, Chat_Receive_Response,
            Chat_Receive_Response> {

        String my_fb_id, his_fb_id;
        int position;

        public get_chat_receive_list(String my_fb_id, String his_fb_id, int position) {
            this.my_fb_id = my_fb_id;
            this.his_fb_id = his_fb_id;
            this.position = position;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Chat_Receive_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_Receive_Response> call = a.get_receive_chat(my_fb_id, his_fb_id);
            Chat_Receive_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Receive_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Chat_Receive_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_receive_list.clear();
                    Stored_datas.getInstance().setChat_his_fb_id(c.to_fb_id);
                    Stored_datas.getInstance().setChat_his_name(c.to_user_name);
                    Stored_datas.getInstance().setChat_his_image(c.to_user_photo);
                    Log.e(DATIFY_LOG, "Chat_Name_Before: " + Stored_datas.getInstance().getChat_his_name());

                    if (null != c.chat_list_results && c.chat_list_results.size() > 0) {
                        for (Chat_Receive_Response.Chat_List_Result dd : c.chat_list_results) {

                            chat_receive_list_model = new Chat_Receive_List(dd.chat_from_fb_id, dd.chat_to_fb_id,
                                    dd.chat_message, dd.chat_date, dd.chat_time);
                            chat_receive_list.add(chat_receive_list_model);
                                    }
                                }

                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(chat_receive_list);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(My_Matches_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                                } else if (c.status == 0) {
                    Stored_datas.getInstance().setChat_his_fb_id(his_fb_id);
                    Stored_datas.getInstance().setChat_his_name(my_matches_list.get(position).getFullname());
                    Stored_datas.getInstance().setChat_his_image(my_matches_list.get(position).getProfilepicture());
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(null);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(My_Matches_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                            }
                        }}
                    }
                }
