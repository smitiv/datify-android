package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Models.Recent_Views_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Chat_Receive_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Recent_Views extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id, version;

    ArrayList<Recent_Views_List> recent_views_list;
    RecyclerView recent_views_recyclerview;
    RecyclerView.Adapter recent_view_adapter;
    RelativeLayout no_data_layout;

    ImageView back_arrow;
    Toolbar toolbar;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    Retrofit retrofit_obj;
    String mess;
    AlertDialog dialog;
    ArrayList<Chat_Receive_List> chat_receive_list;
    Chat_Receive_List chat_receive_list_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recent_views_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Recent_Views.this);
        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Recent_Views.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                     } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    private void init_views() {
        dialog = new SpotsDialog(Recent_Views.this);
        chat_receive_list = new ArrayList<Chat_Receive_List>();
        recent_views_list = new ArrayList<Recent_Views_List>();
        recent_views_list = Stored_datas.getInstance().getRecent_views_list();
        no_data_layout = (RelativeLayout) findViewById(R.id.no_data_layout);
        recent_views_recyclerview = (RecyclerView) findViewById(R.id.recent_views_recyclerview);
        if(recent_views_list != null) {
            no_data_layout.setVisibility(View.GONE);
            if (recent_views_list.size() > 0) {
                recent_views_recyclerview.setHasFixedSize(true);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(Recent_Views.this);    // LinearLayout for Listview
                recent_views_recyclerview.setLayoutManager(manager);
                recent_view_adapter = new Recent_Views_Adapter(getApplicationContext(), recent_views_list);
                recent_views_recyclerview.setAdapter(recent_view_adapter);
                recent_view_adapter.notifyDataSetChanged();
                no_data_layout.setVisibility(View.GONE);
                            } else {
                no_data_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
            no_data_layout.setVisibility(View.VISIBLE);
                    }
                }

    @Override
    public void onBackPressed() {
        setResult(RECENT_VIEW_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {

                }
            }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
                    }

    class Recent_Views_Adapter extends RecyclerView.Adapter<Recent_Views_Adapter.ViewHolder> {
        Context context;
        ArrayList<Recent_Views_List> recent_views_list;

        public Recent_Views_Adapter(Context context,  ArrayList<Recent_Views_List> recent_views_list) {
            this.context = context;
            this.recent_views_list = recent_views_list;
                }

        @Override
        public Recent_Views_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_views_items, parent, false);
            return new Recent_Views_Adapter.ViewHolder(view);
                }

        @Override
        public void onBindViewHolder(Recent_Views_Adapter.ViewHolder holder, final int position) {
            holder.r_view_name.setText(recent_views_list.get(position).getFullname());

            if(recent_views_list.get(position).getJob_position().equalsIgnoreCase("")) {
                holder.r_view_age.setText("Age: "+recent_views_list.get(position).getAge());
                        } else {
                holder.r_view_age.setText(recent_views_list.get(position).getAge() + ", ");
                    }
            holder.r_view_designation.setText(recent_views_list.get(position).getJob_position());
            holder.r_view_hours.setText(recent_views_list.get(position).getViewed_date());
            Glide.with(context).load(recent_views_list.get(position).getProfilepicture()).into(holder.recent_view_user_image);
            holder.r_view_chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cd = new ConnectionDetector(getApplicationContext());
                    isInternetPresent = cd.isConnectingToInternet();
                    if (!isInternetPresent) {
                        Snackbar.make(view, "No Internet Connection", Snackbar.LENGTH_LONG).
                                setAction("Action", null).show();
                        return;
                            }

                    mess = APIUrl.BASE_URL;
                    retrofit_obj = new Retrofit.Builder()
                            .baseUrl(mess)
                            .addConverterFactory(GsonConverterFactory.create()).build();
                    new get_chat_receive_list(str_user_fb_id, recent_views_list.get(0).getUser_fb_id(), position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        });
                    }

        @Override
        public int getItemCount() {
            return recent_views_list.size();
                    }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView recent_view_user_image;
            TextView r_view_name, r_view_age, r_view_designation, r_view_hours;
            ImageView r_view_chat;

            public ViewHolder(View itemView) {
                super(itemView);
                r_view_name = (TextView)itemView.findViewById(R.id.r_view_name);
                r_view_age = (TextView)itemView.findViewById(R.id.r_view_age);
                r_view_designation = (TextView)itemView.findViewById(R.id.r_view_designation);
                r_view_hours = (TextView)itemView.findViewById(R.id.r_view_hours);
                recent_view_user_image = (ImageView)itemView.findViewById(R.id.recent_view_user_image);
                r_view_chat = (ImageView)itemView.findViewById(R.id.r_view_chat);
                                }
                            }
                        }

    private class get_chat_receive_list extends AsyncTask<Void, Chat_Receive_Response,
            Chat_Receive_Response> {

        String my_fb_id, his_fb_id;
        int position;

        public get_chat_receive_list(String my_fb_id, String his_fb_id, int position) {
            this.my_fb_id = my_fb_id;
            this.his_fb_id = his_fb_id;
            this.position = position;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Chat_Receive_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_Receive_Response> call = a.get_receive_chat(my_fb_id, his_fb_id);
            Chat_Receive_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Receive_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Chat_Receive_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_receive_list.clear();
                    Stored_datas.getInstance().setChat_his_fb_id(c.to_fb_id);
                    Stored_datas.getInstance().setChat_his_name(c.to_user_name);
                    Stored_datas.getInstance().setChat_his_image(c.to_user_photo);
                    Log.e(DATIFY_LOG, "Chat_Name_Before: " + Stored_datas.getInstance().getChat_his_name());

                    if (null != c.chat_list_results && c.chat_list_results.size() > 0) {
                        for (Chat_Receive_Response.Chat_List_Result dd : c.chat_list_results) {

                            chat_receive_list_model = new Chat_Receive_List(dd.chat_from_fb_id, dd.chat_to_fb_id,
                                    dd.chat_message, dd.chat_date, dd.chat_time);
                            chat_receive_list.add(chat_receive_list_model);
                                    }
                                }

                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(chat_receive_list);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(Recent_Views.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                } else if (c.status == 0) {
                    Stored_datas.getInstance().setChat_his_fb_id(his_fb_id);
                    Stored_datas.getInstance().setChat_his_name(recent_views_list.get(position).getFullname());
                    Stored_datas.getInstance().setChat_his_image(recent_views_list.get(position).getProfilepicture());
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(null);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(Recent_Views.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                                }
                            }}
                        }
                }
