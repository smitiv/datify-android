package com.smitiv.datify.Activities;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.smitiv.datify.Dashboard_Classes.Dashboard;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.R;
import com.smitiv.datify.Sessions.Stored_datas;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash_Screen extends AppCompatActivity implements Intent_Constants {

    ImageView datify_logo;
    private static int SPLASH_TIME_OUT = 2000;
    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);

        /*// Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.smitiv.datify",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                    }
                } catch (PackageManager.NameNotFoundException e) {
                } catch (NoSuchAlgorithmException e) {
                }*/

        init_views();
          }

    private void init_views() {
        datify_logo = (ImageView) findViewById(R.id.datify_logo);

        new Handler().postDelayed(new Runnable() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                pref = PreferenceManager.getDefaultSharedPreferences(Splash_Screen.this);
                editor = pref.edit();
                str_user_fb_id = pref.getString("datify_id","");
                editor.commit();

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                      }

                if(str_user_fb_id.equalsIgnoreCase("")) {

                    Stored_datas.getInstance().setLoginstatus("1");
                    Intent intent = new Intent(Splash_Screen.this, Login_Screen.class);
                    Pair[] pair = new Pair[1];
                    pair[0] = new Pair<View, String>(datify_logo, "applogo_transition");

                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(Splash_Screen.this, pair);
                    startActivity(intent, options.toBundle());
                            } else {
                    Stored_datas.getInstance().setLoginstatus("2");
                    Intent intent = new Intent(Splash_Screen.this, Dashboard.class);
                    startActivity(intent);
                    finish();
                            }
                        }
                    }, SPLASH_TIME_OUT);
                }
        }
