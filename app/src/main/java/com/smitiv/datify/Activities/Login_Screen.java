package com.smitiv.datify.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.internal.gmsg.HttpClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Dashboard_Classes.Dashboard;
import com.smitiv.datify.GPS_Tracker.GPSTracker;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Edit_Profile_Response;
import com.smitiv.datify.Response.Login_Response;
import com.smitiv.datify.Sessions.Stored_datas;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import dmax.dialog.SpotsDialog;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener
        ,DatePickerDialog.OnDateSetListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    LinearLayout fb_layout;
    LoginButton fb_login_button;
    String fb_id, name, email1, gender, profile_img, birthday;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
                    }
        @Override
        public void onCancel() {
                    }
        @Override
        public void onError(FacebookException e) {
                    }
                };

    // Getting Location
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    GPSTracker gps;
    double latitude;
    double longitude;
    String  plat = "", plon = "", str_user_dob="", str_gender="";

    String android_id;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    //Retrofit
    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    DatePickerDialog check_in_dpd;
    TextView dob_link;
    private static final String[] gender_array = {"Male", "Female"};
    private ArrayAdapter<String> gender_adapter;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_screen);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
                    }

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        pref = PreferenceManager.getDefaultSharedPreferences(Login_Screen.this);
        Log.e(DATIFY_LOG, "Android_id: " + android_id);

        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        init_views();
        fb_activities();
//        mappermission();
             }

    /*private void mappermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getApplicationContext(), Login_Screen.this)) {
                fetchLocationData();
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSION_REQUEST_CODE_LOCATION,
                        getApplicationContext(),Login_Screen.this);
            }
        } else {
            fetchLocationData();
        }
    }

    public void requestPermission(String strPermission, int perCode, Context _c, Activity _a) {
        ActivityCompat.requestPermissions(_a, new String[]{strPermission}, perCode);
                    }

    public static boolean checkPermission(String strPermission, Context _c, Activity _a) {
        int result = ContextCompat.checkSelfPermission(_c, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
                } else {
            return false;
                }
            }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocationData();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
                    }
                }*/

    /*private void fetchLocationData() {
        gps = new GPSTracker(Login_Screen.this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            plat = String.valueOf(latitude);
            plon = String.valueOf(longitude);

            Log.e(DATIFY_LOG,"Latitude: " + plat);
            Log.e(DATIFY_LOG,"Longitude: " + plon);

            if (Double.compare(latitude, Double.NaN) == 0 && Double.compare(longitude, Double.NaN) == 0) {
                    } else {
                getAddress(latitude, longitude);
                    }
                } else {
            gps.showSettingsAlert();
                }
            }*/

    /*private void getAddress(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(Login_Screen.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Address obj = addresses.get(0);
                String add = obj.getAddressLine(0);
                add = add + "\n" + obj.getCountryName();
                add = add + "\n" + obj.getCountryCode();
                add = add + "\n" + obj.getAdminArea();
                add = add + "\n" + obj.getPostalCode();
                add = add + "\n" + obj.getSubAdminArea();
                add = add + "\n" + obj.getLocality();
                add = add + "\n" + obj.getSubThoroughfare();
            } else {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    private void init_views() {
        fb_layout = (LinearLayout) findViewById(R.id.fb_layout);
        fb_layout.setOnClickListener(this);
                }

    private void fb_activities() {
        fb_login_button = (LoginButton) findViewById(R.id.fb_login_button);
        List<String> permissionNeeds = Arrays.asList("user_photos", "email", "user_birthday", "public_profile");
        fb_login_button.setReadPermissions(permissionNeeds);
        fb_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
//                setFacebookData(loginResult);
                Log.e("LoginActivity", loginResult.toString());
                System.out.println("onSuccess");
                GraphRequest request = GraphRequest.newMeRequest
                        (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("LoginActivity", response.toString());
                                Log.v("LoginActivity", response.toString());
                                try {
                                    Log.e(DATIFY_LOG, "Result: " + object.toString());
                                    fb_id = object.getString("id");
                                    name = object.getString("name");
                                    String a[]=name.split(" ");
                                    String f_name=a[0];
                                    String l_name=a[1];
                                    if(object.has("email")) {
                                        email1 = object.getString("email");
                                              } else {
                                        email1 = object.getString("id");
                                            }

                                    if(object.has("gender")) {
                                        gender = object.getString("gender");
                                                } else {
                                        gender = "Male";
                                                }

                                    if(object.has("birthday")) {
                                        birthday = object.getString("birthday");
                                                } else {
                                        birthday = "1990-09-07";
                                            }

                                    if(fb_id != "" && fb_id != null) {
                                        profile_img = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                                              }

                                    AccessToken token = AccessToken.getCurrentAccessToken();
//                                    getCoverPhotoFB(email1, token);

                                    /*new GraphRequest(
                                            AccessToken.getCurrentAccessToken(),
                                            "https://graph.facebook.com/" + fb_id + "?fields=cover&access_token=" + AccessToken.getCurrentAccessToken() ,
                                            null,
                                            HttpMethod.GET,
                                            new GraphRequest.Callback() {
                                                public void onCompleted(GraphResponse response1) {
                                                    Log.e(DATIFY_LOG, response1.toString());
                                                        }
                                                    }
                                            ).executeAsync();*/

                                    /*new GraphRequest(
                                            AccessToken.getCurrentAccessToken(),
                                            "https://graph.facebook.com/" +fb_id+"?fields={cover}",
                                            null,
                                            HttpMethod.GET,
                                            new GraphRequest.Callback()
                                            {
                                                public void onCompleted(GraphResponse response)
                                                {
                                                    Log.e(DATIFY_LOG, response.toString());
                                                    try
                                                    {
                                                        JSONObject jsonObject = response.getJSONObject();
                                                        if(jsonObject==null)
                                                            return;
                                                        JSONObject JOSource = jsonObject.getJSONObject("cover");
                                                        String coverPhoto = JOSource.getString("source");
                                                        Log.e(DATIFY_LOG, "Cover: " + coverPhoto);

                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                    ).executeAsync();*/

                                    Log.e(DATIFY_LOG, "Token: " + token.getToken());
                                    LoginManager.getInstance().logOut();
                                    Log.e(DATIFY_LOG, "First_Name: " + f_name);
                                    Log.e(DATIFY_LOG, "Last_Name: " + l_name);
                                    Log.e(DATIFY_LOG, "Name: " + name);
                                    Log.e(DATIFY_LOG, "FB_id: " + fb_id);
                                    Log.e(DATIFY_LOG, "Email: " + email1);
                                    Log.e(DATIFY_LOG, "Gender: " + gender);
                                    Log.e(DATIFY_LOG, "Image: " + profile_img);
                                    Log.e(DATIFY_LOG, "Access_Token: " + loginResult.getAccessToken().getToken().toString());

//                                    getCoverPhotoFB(email1, loginResult.getAccessToken());

                                    cd = new ConnectionDetector(getApplicationContext());
                                    isInternetPresent = cd.isConnectingToInternet();
                                    if (!isInternetPresent) {
                                        Snackbar.make(fb_login_button, "No Internet Connection", Snackbar.LENGTH_LONG).
                                                setAction("Action", null).show();
                                        return;
                                          }

                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF),
                                            Context.MODE_PRIVATE);
                                    String fcm_token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");

                                    Stored_datas.getInstance().setReg_name(name);
                                    Stored_datas.getInstance().setReg_fb_id(fb_id);
                                    Stored_datas.getInstance().setReg_email(email1);
                                    Stored_datas.getInstance().setReg_profile_img(profile_img);
                                    Stored_datas.getInstance().setReg_device_token(fcm_token);
                                    Stored_datas.getInstance().setReg_device_type("1");
                                    Stored_datas.getInstance().setReg_latitude(plat);
                                    Stored_datas.getInstance().setReg_longitude(plon);

//                                    show_dialog(name, fb_id, email1, profile_img, fcm_token, "1", plat, plon);

                                    Log.e(DATIFY_LOG, "Fcm_Token: " + fcm_token);
                                    mess = APIUrl.BASE_URL;
                                    retrofit_obj = new Retrofit.Builder()
                                            .baseUrl(mess)
                                            .addConverterFactory(GsonConverterFactory.create()).build();
                                    new fb_login(fb_id, name, email1, profile_img, "", fcm_token, "1",
                                            plat,  plon, "", "").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                        } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                                    Log.e("Exception", e.getMessage());
                                    e.printStackTrace();
                                      }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
                            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });
    }

    private void show_dialog(final String name,final String fb_id,final String email,final String profile_img,
                             final String fcm_token,final String device_type,final String plat,final String plon) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.registration_dialog, null);
        final android.support.v7.app.AlertDialog.Builder alert_Dialog = new android.support.v7.app.AlertDialog.Builder(Login_Screen.this, R.style.FullScreenDialogStyle);
        alert_Dialog.setView(alertLayout);

        alert_Dialog.setCancelable(false);

        final Dialog dialog = alert_Dialog.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        ImageView close_btn = (ImageView) alertLayout.findViewById(R.id.close_btn);
        TextView name_text = (TextView) alertLayout.findViewById(R.id.name_text);
        name_text.setText(name);

        dob_link = (TextView) alertLayout.findViewById(R.id.dob_link);
        TextView register_here = (TextView) alertLayout.findViewById(R.id.register_here);

        MaterialSpinner gender_spinner = (MaterialSpinner) alertLayout.findViewById(R.id.gender_spinner);
        gender_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, gender_array);
        gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender_spinner.setAdapter(gender_adapter);
        gender_spinner.setPaddingSafe(0, 0, 0, 0);

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_gender = parent.getItemAtPosition(position).toString();
                Log.e(DATIFY_LOG, "Gender_Data: " + str_gender);
                if(str_gender.equalsIgnoreCase("Gender")) {
                    str_gender = "";
                       }
                     }
            public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dob_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                check_in_dpd = DatePickerDialog.newInstance(
                        Login_Screen.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));

                int updated_year = (Calendar.YEAR) - 18;
                int updated_month = Calendar.MONTH;

                check_in_dpd.setThemeDark(true); //set dark them for dialog?
                check_in_dpd.vibrate(false); //vibrate on choosing date?
                check_in_dpd.dismissOnPause(true); //dismiss dialog when onPause() called?
                check_in_dpd.showYearPickerFirst(false); //choose year first?
                check_in_dpd.setAccentColor(Color.parseColor("#9C27A0")); // custom accent color
                check_in_dpd.setTitle("Please select a date"); //dialog title
                Calendar max_calendar = Calendar.getInstance();
                max_calendar.set(1998, 5,01);
                check_in_dpd.setMaxDate(max_calendar);
                check_in_dpd.show(getFragmentManager(), "Date-of-Birth...");
                         }
                    });
        register_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str_gender.equalsIgnoreCase("")) {
                    Snackbar.make(fb_login_button, "Please select your gender", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                       }

                if(str_user_dob.equalsIgnoreCase("")) {
                    Snackbar.make(fb_login_button, "Please update your DOB", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                       }

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(fb_login_button, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }
                dialog.cancel();
                Log.e(DATIFY_LOG, "Fcm_Token: " + fcm_token);

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new update_profile(fb_id, "", "","", "", "", str_gender, str_user_dob).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                /*mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                   .baseUrl(mess)
                   .addConverterFactory(GsonConverterFactory.create()).build();
                new fb_login(fb_id, name, email, profile_img, str_user_dob, fcm_token, device_type,
                   plat,  plon, str_gender, "").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);*/
                        }
                    });
            dialog.show();
                 }


    public void getCoverPhotoFB(final String email, AccessToken accessToken){

        if(!AccessToken.getCurrentAccessToken().getPermissions().contains("user_photos")) {
            Log.e(DATIFY_LOG, "getCoverPhotoFB....get user_photo permission");
            LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    Arrays.asList("user_photos"));
                    }

        Bundle params = new Bundle();
        params.putBoolean("redirect", false);
        params.putString("fields", "cover");
        new GraphRequest(
                accessToken,
                "me",
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(final GraphResponse response) {
                        Log.e(DATIFY_LOG, "getCoverPhotoFB..."+response);

                        // thread is necessary for network call
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {

                                try {

                                    String picUrlString = (String) response.getJSONObject().getJSONObject("cover").get("source");
                                    Log.d(DATIFY_LOG,"getCoverPhotoFB.....picURLString....." + picUrlString);
                                    URL img_value = new URL(picUrlString);
                                    Bitmap eventBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
//                                    saveImageToExternalStorage(eventBitmap, email + "_B.png");

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }

                }
        ).executeAsync();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        str_user_dob =  year + "-" + (++monthOfYear) + "-" +  dayOfMonth;
        dob_link.setText(str_user_dob);
                    }

    private class update_profile extends AsyncTask<Void, Edit_Profile_Response,
            Edit_Profile_Response> {

        AlertDialog dialog = new SpotsDialog(Login_Screen.this);
        String fb_id, str_designation, str_about_me, str_weight, str_height,
                str_marital_status, str_gender, str_user_dob;

        public update_profile(String fb_id, String str_designation, String str_about_me, String str_weight,
                              String str_height, String str_marital_status, String str_gender, String str_user_dob) {
            this.fb_id = fb_id;
            this.str_designation = str_designation;
            this.str_about_me = str_about_me;
            this.str_weight = str_weight;
            this.str_height = str_height;
            this.str_marital_status = str_marital_status;
            this.str_gender = str_gender;
            this.str_user_dob = str_user_dob;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Edit_Profile_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Edit_Profile_Response> call = a.update_profile(fb_id, "", "", "",
                    "", "", "", "", "", str_user_dob, str_gender);
            Edit_Profile_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Update_Profile_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Edit_Profile_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();
                    editor = pref.edit();
                    editor.putString("datify_dob", str_user_dob);
                    editor.putString("datify_gender", str_gender);
                    editor.putString("datify_id", c.fb_id);
                    editor.putString("datify_name", c.fullname);
                    editor.putString("datify_email", c.email);
                    editor.putString("datify_profile_img", c.profilepicture);
                    editor.putString("datify_dob", c.dob);
                    editor.putString("datify_gender", c.gender);
                    editor.putString("datify_looking_for", c.looking_for);
                    editor.putString("datify_distance", c.distance);
                    editor.putString("datify_alerts", c.alerts);
                    editor.putString("datify_age", c.age);
                    editor.putString("datify_cover_photo", "");
                    editor.commit();

                    Toast.makeText(getApplicationContext(), "Successfully Registered", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login_Screen.this, Dashboard.class);
                    startActivityForResult(intent, LOGIN_to_DASHBOARD);


                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Login_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private class fb_login extends AsyncTask<Void, Login_Response,
            Login_Response> {

        AlertDialog dialog = new SpotsDialog(Login_Screen.this);
        String fb_id, name, email, profile_img, dob, device_token, device_type, latitude, longitude, gender, str_cover_photo;

        public fb_login(String fb_id, String name, String email, String profile_img, String dob, String device_token,
                                   String device_type, String latitude, String longitude, String gender,
                                   String str_cover_photo) {
            this.fb_id = fb_id;
            this.name = name;
            this.email = email;
            this.profile_img = profile_img;
            this.dob = dob;
            this.device_token = device_token;
            this.device_type = device_type;
            this.latitude = latitude;
            this.longitude = longitude;
            this.gender = gender;
            this.str_cover_photo = str_cover_photo;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Login_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Login_Response> call = a.fb_login(fb_id, name, email, profile_img, dob, device_token, device_type,
                    latitude, longitude, gender, str_cover_photo);

            Login_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "FB_Login_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Login_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {

                    dialog.dismiss();
                    if(c.msg.equalsIgnoreCase("Logged in success")) {

                        editor = pref.edit();
                        editor.putString("datify_id", c.fb_id);
                        editor.putString("datify_name", c.fullname);
                        editor.putString("datify_email", c.email);
                        editor.putString("datify_profile_img", c.profilepicture);
                        editor.putString("datify_dob", c.dob);
                        editor.putString("datify_gender", c.gender);
                        editor.putString("datify_looking_for", c.looking_for);
                        editor.putString("datify_distance", c.distance);
                        editor.putString("datify_alerts", c.alerts);
                        editor.putString("datify_age", c.age);
                        editor.putString("datify_cover_photo", c.cover_photo_link);
                        editor.commit();

                        Log.e(DATIFY_LOG, "Looking_for: " + c.looking_for);
                        Log.e(DATIFY_LOG, "Distance: " + c.distance);
                        Log.e(DATIFY_LOG, "Alerts: " + c.alerts);

                        Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Login_Screen.this, Dashboard.class);
                        startActivityForResult(intent, LOGIN_to_DASHBOARD);
                                } else {
                        show_dialog(name, fb_id, email1, profile_img, device_token, "1", plat, plon);
                            }

                } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Login_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }

    private void displayMessage(Profile profile){
        if(profile != null){
            Log.e("ProfName: ",profile.getName());
                    }
                }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
                    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection Suspended!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    public void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Login_Screen.this, 1000);
                                    } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                                }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                            }
                         }
                    });
                }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
                    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(Login_Screen.this);
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.fb_layout:
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(fb_login_button, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                      }

                LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {}

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch(Exception ex) {}

                if(!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
                    dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            paramDialogInterface.dismiss();
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                                    }
                                });
                    dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            paramDialogInterface.dismiss();
                                    }
                                });
                        dialog.show();
                        } else {
//                    fetchLocationData();
                    if (mGoogleApiClient != null) {
                        mGoogleApiClient.connect();
                            }
                        onConnected(null);
                        settingRequest();
                        fb_login_button.performClick();
                            }
                        break;
                        }
                    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    // All required changes were successfully made
                    Log.e("location", "RESULT_OK");
                    getLocation();
                    break;
                case Activity.RESULT_CANCELED:
                    // The user was asked to change settings, but chose not to
                    Toast.makeText(this, "Location Service not Enabled", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOGIN_to_DASHBOARD) {
            if(resultCode == LOGOUT) {
                Toast.makeText(getApplicationContext(), "You are successfully logged out", Toast.LENGTH_SHORT).show();
                        }
                    }
        callbackManager.onActivityResult(requestCode, resultCode, data);
                    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {
                Log.e(DATIFY_LOG, "getLocation");
                Log.e(DATIFY_LOG, "Latitude: " + String.valueOf(mLastLocation.getLatitude()));
                Log.e(DATIFY_LOG, "Longitude: " + String.valueOf(mLastLocation.getLongitude()));
                plat = String.valueOf(mLastLocation.getLatitude());
                plon = String.valueOf(mLastLocation.getLongitude());
            } else {
                /*if there is no last known location. Which means the device has no data for the loction currently.
                * So we will get the current location.
                * For this we'll implement Location Listener and override onLocationChanged*/
                Log.e(DATIFY_LOG, "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, Login_Screen.this);
                        }
                    }
                }

    /*When Location changes, this method get called. */
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        Log.e(DATIFY_LOG, "onLocationChanged");
        plat = String.valueOf(mLastLocation.getLatitude());
        plon = String.valueOf(mLastLocation.getLongitude());
        Log.e(DATIFY_LOG, "Latitude: " + String.valueOf(mLastLocation.getLatitude()));
        Log.e(DATIFY_LOG, "Longitude: " + String.valueOf(mLastLocation.getLongitude()));
                    }

}
