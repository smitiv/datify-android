package com.smitiv.datify.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Profile_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Sessions.Stored_datas;

import java.util.ArrayList;

import retrofit2.Retrofit;

public class New_My_Profile_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id,
            str_cover_photo, version;

    ImageView back_arrow;
    private Toolbar toolbar;

    ImageView profi_cover_photo;
    CircularImageView profi_user_image;
    TextView profi_name, profi_age, profi_designation, profi_about_me, profi_weight,
            profi_height, married_status;
    ArrayList<Profile_List> profile_list;
    LinearLayout edit_profile_link;

    String str_name, str_age, str_designation, str_about_me, str_education_value,
            str_marital_status, str_interest, str_weight, str_height, str_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_my_profile);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(New_My_Profile_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                        }
                    });
                }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(New_My_Profile_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        str_cover_photo = pref.getString("datify_cover_photo","");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                     } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    private void init_views() {
        profi_cover_photo = (ImageView) findViewById(R.id.profi_cover_photo);
        profi_user_image = (CircularImageView) findViewById(R.id.profi_user_image);

        profi_name = (TextView) findViewById(R.id.profi_name);
        profi_age = (TextView) findViewById(R.id.profi_age);
        profi_designation = (TextView) findViewById(R.id.profi_designation);
        profi_about_me = (TextView) findViewById(R.id.profi_about_me);
        edit_profile_link = (LinearLayout) findViewById(R.id.edit_profile_link);
        profi_weight = (TextView) findViewById(R.id.profi_weight);
        profi_height = (TextView) findViewById(R.id.profi_height);
        married_status = (TextView) findViewById(R.id.married_status);
        profile_list = new ArrayList<Profile_List>();
        profile_list = Stored_datas.getInstance().getProfile_list();
        edit_profile_link.setOnClickListener(this);
        profi_user_image.setOnClickListener(this);
        profi_cover_photo.setOnClickListener(this);
        add_values();
                }

    private void add_values() {
        if(profile_list.size()> 0) {
            str_name = profile_list.get(0).getFullname();
            str_age = profile_list.get(0).getAge();
            str_designation = profile_list.get(0).getJob_position();
            str_about_me = profile_list.get(0).getAbout_me();
            str_education_value = profile_list.get(0).getEducation();
            str_marital_status = profile_list.get(0).getMarital_status();
            str_interest = profile_list.get(0).getInterest();
            str_weight = profile_list.get(0).getWeight();
            str_height = profile_list.get(0).getHeight();
            str_location = profile_list.get(0).getLocation();

            values_validation();
                    }
                }

    private void values_validation() {

        if(!str_age.equalsIgnoreCase("")) {
            profi_age.setText(str_age);
                    } else {
            profi_age.setText("");
                    }

        if(!str_name.equalsIgnoreCase("")) {
            if(str_age.equalsIgnoreCase("")) {
                profi_name.setText(str_name);
                        } else {
                profi_name.setText(str_name + ", ");
                    }
                }

        if(!str_designation.equalsIgnoreCase("")) {
            profi_designation.setText(str_designation);
                    } else {
            profi_designation.setText("");
                }

        if(!str_about_me.equalsIgnoreCase("")) {
            profi_about_me.setText(str_about_me);
                } else {
            profi_about_me.setVisibility(View.GONE);
             }

        if(!str_marital_status.equalsIgnoreCase("")) {
            married_status.setText(str_marital_status);
                    } else {
            married_status.setText("");
                }

        if(!str_weight.equalsIgnoreCase("")) {
            profi_weight.setText(str_weight);
                    } else {
            profi_weight.setText("");
                }

        if(!str_height.equalsIgnoreCase("")) {
            profi_height.setText(str_height);
                    } else {
            profi_height.setText("");
                }

        if(str_cover_photo != null) {
            if(!str_cover_photo.equalsIgnoreCase("")) {
                Glide.with(getApplicationContext()).load(str_cover_photo).into(profi_cover_photo);
                        }
                    }

         if(str_user_profile_img != null) {
             if (!str_user_profile_img.equalsIgnoreCase("")) {
                 Glide.with(getApplicationContext()).load(str_user_profile_img).into(profi_user_image);
                        }
                    }
                }

    @Override
    public void onBackPressed() {
        setResult(NEW_MY_PROFILE_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.profi_user_image:
                if(str_user_profile_img != null) {
                    if (!str_user_profile_img.equalsIgnoreCase("")) {
                        new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, profi_user_image,
                                str_user_profile_img, null);
                                }
                            }
                break;
            case R.id.profi_cover_photo:
                if(str_cover_photo != null) {
                    if(!str_cover_photo.equalsIgnoreCase("")) {
                        new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, profi_cover_photo,
                                str_cover_photo, null);
                                }
                            }
                break;
            case R.id.edit_profile_link:
                Stored_datas.getInstance().setProf_about_me(str_about_me);
                Stored_datas.getInstance().setProf_designation(str_designation);
                Stored_datas.getInstance().setProf_height(str_height);
                Stored_datas.getInstance().setProf_weight(str_weight);
                Stored_datas.getInstance().setProf_marital_status(str_marital_status);
                Intent intent = new Intent(New_My_Profile_Screen.this, Edit_Profile_Screen.class);
                startActivityForResult(intent, MY_PROFILE_to_EDIT_PROFILE);
                break;
                }
            }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MY_PROFILE_to_EDIT_PROFILE) {
            if(resultCode == EDIT_PROFILE_back) {
                update_photo();
                      }

            if(resultCode == PROFILE_UPDATED) {

                profi_designation.setText(Stored_datas.getInstance().getProf_designation());
                profi_about_me.setText(Stored_datas.getInstance().getProf_about_me());
                profi_weight.setText(Stored_datas.getInstance().getProf_weight());
                profi_height.setText(Stored_datas.getInstance().getProf_height());
                married_status.setText(Stored_datas.getInstance().getProf_marital_status());

                update_photo();
                        }
                    }
                }

    private void update_photo() {
        editor = pref.edit();
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_cover_photo = pref.getString("datify_cover_photo","");
        editor.commit();

        if(str_cover_photo != null) {
            if(!str_cover_photo.equalsIgnoreCase("")) {
                Glide.with(getApplicationContext()).load(str_cover_photo).into(profi_cover_photo);
                            }
                        }

        if(str_user_profile_img != null) {
            if (!str_user_profile_img.equalsIgnoreCase("")) {
                Glide.with(getApplicationContext()).load(str_user_profile_img).into(profi_user_image);
                            }
                        }
                }
}
