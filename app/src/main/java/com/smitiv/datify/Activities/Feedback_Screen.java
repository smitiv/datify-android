package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
//import android.widget.RatingBar;
import android.widget.Toast;
import com.smitiv.datify.Support_Classes.ColoredRatingBar.OnRatingBarChangeListener;

import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Feedback_Response;
import com.smitiv.datify.Support_Classes.ColoredRatingBar;

import java.io.IOException;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Feedback_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    LinearLayout send_loyout;
    ColoredRatingBar rating_bar;
    EditText rating_text;
    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id, version;
    String str_rating = "", str_rating_text;

    ImageView back_arrow;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Feedback_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                        }
                    });
                }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Feedback_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                     } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    private void init_views() {
        rating_text = (EditText) findViewById(R.id.rating_text);
        rating_bar = (ColoredRatingBar) findViewById(R.id.rating_bar);
        send_loyout = (LinearLayout) findViewById(R.id.send_loyout);
        send_loyout.setOnClickListener(this);
        rating_bar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(ColoredRatingBar ratingBar, float rating,
                                        boolean fromUser) {
                str_rating = String.valueOf(rating);
                        }
                    });
                }

    @Override
    public void onBackPressed() {
        setResult(FEEDBACK_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.send_loyout:

                if(str_rating.equalsIgnoreCase("0.0") || str_rating.equalsIgnoreCase("") ) {
                    Toast.makeText(getApplicationContext(), "Please check your rating", Toast.LENGTH_SHORT).show();
                    return;
                        }

                str_rating_text = rating_text.getText().toString().trim();

                if(str_rating_text.equalsIgnoreCase("")) {
                    rating_text.requestFocus();
                    Toast.makeText(getApplicationContext(), "Please enter rating data", Toast.LENGTH_SHORT).show();
                    return;
                        }

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(send_loyout, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                        return;
                          }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new send_my_rating(str_user_fb_id, str_user_email, str_rating_text, str_rating, version).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
                }
            }

    private class send_my_rating extends AsyncTask<Void, Feedback_Response,
            Feedback_Response> {

        AlertDialog dialog = new SpotsDialog(Feedback_Screen.this);
        String str_user_fb_id, str_user_email, str_rating_text, str_rating, version;

        public send_my_rating(String str_user_fb_id, String str_user_email, String str_rating_text, String str_rating,
                              String version) {
            this.str_user_fb_id = str_user_fb_id;
            this.str_user_email = str_user_email;
            this.str_rating_text = str_rating_text;
            this.str_rating = str_rating;
            this.version = version;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Feedback_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Feedback_Response> call = a.send_feedback(str_user_fb_id, str_user_email, str_rating_text,
                    str_rating, version);
            Feedback_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Feedback_Response: " + c);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Feedback_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();

                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                    onBackPressed();
                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Feedback_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
                    }
                }
