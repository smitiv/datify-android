package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Chat_Details_List;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Models.Recent_Views_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Chat_Receive_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Chat_List_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id, version;

    RecyclerView chat_lists_recyclerview;
    RecyclerView.Adapter chat_list_adapter;
    RelativeLayout no_data_layout;

    ImageView back_arrow;
    Toolbar toolbar;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    Retrofit retrofit_obj;
    String mess;
    AlertDialog dialog;
    ArrayList<Chat_Receive_List> chat_receive_list;
    Chat_Receive_List chat_receive_list_model;
    ArrayList<Chat_Details_List> chat_details_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_list_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Chat_List_Screen.this);
        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Chat_List_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                     } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    private void init_views() {
        dialog = new SpotsDialog(Chat_List_Screen.this);
        chat_receive_list = new ArrayList<Chat_Receive_List>();
        chat_details_list = new ArrayList<Chat_Details_List>();

        chat_details_list = Stored_datas.getInstance().getChat_details_list();
        no_data_layout = (RelativeLayout) findViewById(R.id.no_data_layout);
        chat_lists_recyclerview = (RecyclerView) findViewById(R.id.chat_lists_recyclerview);
        if(chat_details_list != null) {
            no_data_layout.setVisibility(View.GONE);
            if (chat_details_list.size() > 0) {
                chat_lists_recyclerview.setHasFixedSize(true);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(Chat_List_Screen.this);    // LinearLayout for Listview
                chat_lists_recyclerview.setLayoutManager(manager);
                chat_list_adapter = new Chat_Lists_Adapter(getApplicationContext(), chat_details_list);
                chat_lists_recyclerview.setAdapter(chat_list_adapter);
                chat_list_adapter.notifyDataSetChanged();
                no_data_layout.setVisibility(View.GONE);
                            } else {
                no_data_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
            no_data_layout.setVisibility(View.VISIBLE);
                    }

            chat_listeners();
                }

    private void chat_listeners() {
        chat_lists_recyclerview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector detector = new GestureDetector(getApplication(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                        }
                    });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && detector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(child);

                    String to_fb_id = chat_details_list.get(position).getChat_user_id();

                         }
                return false;
                    }
            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                        }
            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
                        }
                    });
                }

    @Override
    public void onBackPressed() {
        setResult(CHAT_LIST_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {

                }
            }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CHAT_LIST_to_CHAT) {
            if(resultCode == CHAT_back) {
                chat_details_list = Stored_datas.getInstance().getChat_details_list();
                chat_list_adapter.notifyDataSetChanged();
                            }
                        }
                    }

    class Chat_Lists_Adapter extends RecyclerView.Adapter<Chat_Lists_Adapter.ViewHolder> {
        Context context;
        ArrayList<Chat_Details_List> chat_details_list;

        public Chat_Lists_Adapter(Context context,  ArrayList<Chat_Details_List> chat_details_list) {
            this.context = context;
            this.chat_details_list = chat_details_list;
                }

        @Override
        public Chat_Lists_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_items, parent, false);
            return new Chat_Lists_Adapter.ViewHolder(view);
                        }

        @Override
        public void onBindViewHolder(Chat_Lists_Adapter.ViewHolder holder, final int position) {

            holder.chat_list_user_name.setText(chat_details_list.get(position).getChat_user_name());
            holder.chat_list_user_msg.setText(chat_details_list.get(position).getChat_message());
            holder.chat_list_msg_time.setText(chat_details_list.get(position).getChat_time());

            Glide.with(getApplicationContext()).load(chat_details_list.get(position).getChat_user_photo()).into(holder.chat_list_user_image);
            if(chat_details_list.get(position).getChat_status() != null) {
                if (chat_details_list.get(position).getChat_status().equalsIgnoreCase("sent_msg")) {
                    holder.chat_list_msg_status.setImageResource(R.drawable.msg_outgoing);
                                } else {
                    holder.chat_list_msg_status.setImageResource(R.drawable.msg_incoming);
                            }
                        }

             holder.chat_details_relative.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     String to_fb_id = chat_details_list.get(position).getChat_user_id();

                     cd = new ConnectionDetector(getApplicationContext());
                     isInternetPresent = cd.isConnectingToInternet();
                     if (!isInternetPresent) {
                         Snackbar.make(chat_lists_recyclerview, "No Internet Connection", Snackbar.LENGTH_LONG).
                                 setAction("Action", null).show();
                         return;
                            }

                     mess = APIUrl.BASE_URL;
                     retrofit_obj = new Retrofit.Builder()
                             .baseUrl(mess)
                             .addConverterFactory(GsonConverterFactory.create()).build();
                     new get_chat_receive_list(str_user_fb_id, to_fb_id, position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                      });
            holder.chat_list_user_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(chat_details_list.get(position).getChat_user_photo() != null){
                        if(!chat_details_list.get(position).getChat_user_photo().equalsIgnoreCase("")) {
                            new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, v,
                                    chat_details_list.get(position).getChat_user_photo(), null);
                                            }
                                        }
                                    }
                                });
                            }

        @Override
        public int getItemCount() {
            return chat_details_list.size();
                    }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CircularImageView  chat_list_user_image;
            TextView chat_list_user_name, chat_list_user_msg, chat_list_msg_time;
            ImageView chat_list_msg_status;
            RelativeLayout chat_details_relative;

            public ViewHolder(View itemView) {
                super(itemView);
                chat_details_relative = (RelativeLayout) itemView.findViewById(R.id.chat_details_relative);
                chat_list_user_image = (CircularImageView) itemView.findViewById(R.id.chat_list_user_image);
                chat_list_user_name = (TextView) itemView.findViewById(R.id.chat_list_user_name);
                chat_list_user_msg = (TextView) itemView.findViewById(R.id.chat_list_user_msg);
                chat_list_msg_time = (TextView) itemView.findViewById(R.id.chat_list_msg_time);
                chat_list_msg_status = (ImageView)itemView.findViewById(R.id.chat_list_msg_status);
                                }
                            }
                        }

    private class get_chat_receive_list extends AsyncTask<Void, Chat_Receive_Response,
            Chat_Receive_Response> {

        String my_fb_id, his_fb_id;
        int position;

        public get_chat_receive_list(String my_fb_id, String his_fb_id, int position) {
            this.my_fb_id = my_fb_id;
            this.his_fb_id = his_fb_id;
            this.position = position;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Chat_Receive_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_Receive_Response> call = a.get_receive_chat(my_fb_id, his_fb_id);
            Chat_Receive_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Receive_Resp: " + c.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return c;
        }

        @Override
        protected void onPostExecute(Chat_Receive_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_receive_list.clear();
                    Stored_datas.getInstance().setChat_his_fb_id(c.to_fb_id);
                    Stored_datas.getInstance().setChat_his_name(c.to_user_name);
                    Stored_datas.getInstance().setChat_his_image(c.to_user_photo);
                    Log.e(DATIFY_LOG, "Chat_Name_Before: " + Stored_datas.getInstance().getChat_his_name());

                    if (null != c.chat_list_results && c.chat_list_results.size() > 0) {
                        for (Chat_Receive_Response.Chat_List_Result dd : c.chat_list_results) {

                            chat_receive_list_model = new Chat_Receive_List(dd.chat_from_fb_id, dd.chat_to_fb_id,
                                    dd.chat_message, dd.chat_date, dd.chat_time);
                            chat_receive_list.add(chat_receive_list_model);
                                    }
                                }

                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_list_position("" + position);
                    Stored_datas.getInstance().setChat_details_list(chat_details_list);
                    Stored_datas.getInstance().setChat_receive_list(chat_receive_list);
                    Stored_datas.getInstance().setChat_from_status("1");
                    Intent chat_intent = new Intent(Chat_List_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, CHAT_LIST_to_CHAT);
                } else if (c.status == 0) {
                    Stored_datas.getInstance().setChat_his_fb_id(his_fb_id);
                    Stored_datas.getInstance().setChat_his_name(chat_details_list.get(position).getChat_user_name());
                    Stored_datas.getInstance().setChat_his_image(chat_details_list.get(position).getChat_user_photo());
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(null);
                    Stored_datas.getInstance().setChat_from_status("1");
                    Intent chat_intent = new Intent(Chat_List_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, CHAT_LIST_to_CHAT);
                                }
                            }}
                        }
                }
