package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Dashboard_Classes.Dashboard;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Models.Filtered_Matches_List;
import com.smitiv.datify.Models.Profile_List;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Chat_Receive_Response;
import com.smitiv.datify.Response.Filtered_Matches_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class New_His_Profile_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id, version;

    ImageView back_arrow;
    private Toolbar toolbar;

    ImageView profi_cover_photo;
    CircularImageView profi_user_image;
    TextView profi_name, profi_age, profi_designation, profi_about_me, profi_weight,
            profi_height, married_status;
    ArrayList<Profile_List> profile_list;
    LinearLayout chat_link;
    ArrayList<Chat_Receive_List> chat_receive_list;
    Chat_Receive_List chat_receive_list_model;
    AlertDialog dialog;

    String str_name, str_age, str_designation, str_about_me, str_education_value,
            str_marital_status, str_interest, str_weight, str_height, str_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_his_profile);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(New_His_Profile_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
             }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                        }
                    });
                }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(New_His_Profile_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
                     } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
                    }
                }

    private void init_views() {
        dialog = new SpotsDialog(New_His_Profile_Screen.this);
        profi_cover_photo = (ImageView) findViewById(R.id.profi_cover_photo);
        profi_user_image = (CircularImageView) findViewById(R.id.profi_user_image);
        profi_user_image.setOnClickListener(this);
        profi_cover_photo.setOnClickListener(this);

        profi_name = (TextView) findViewById(R.id.profi_name);
        profi_age = (TextView) findViewById(R.id.profi_age);
        profi_designation = (TextView) findViewById(R.id.profi_designation);
        profi_about_me = (TextView) findViewById(R.id.profi_about_me);
        chat_link = (LinearLayout) findViewById(R.id.chat_link);
        profi_weight = (TextView) findViewById(R.id.profi_weight);
        profi_height = (TextView) findViewById(R.id.profi_height);
        married_status = (TextView) findViewById(R.id.married_status);
        profile_list = new ArrayList<Profile_List>();
        profile_list = Stored_datas.getInstance().getProfile_list();
        chat_link.setOnClickListener(this);
        chat_receive_list = new ArrayList<Chat_Receive_List>();
        add_values();
                }

    private void add_values() {
        if(profile_list.size()> 0) {
            str_name = profile_list.get(0).getFullname();
            str_age = profile_list.get(0).getAge();
            str_designation = profile_list.get(0).getJob_position();
            str_about_me = profile_list.get(0).getAbout_me();
            str_education_value = profile_list.get(0).getEducation();
            str_marital_status = profile_list.get(0).getMarital_status();
            str_interest = profile_list.get(0).getInterest();
            str_weight = profile_list.get(0).getWeight();
            str_height = profile_list.get(0).getHeight();
            str_location = profile_list.get(0).getLocation();

            values_validation();
                    }
                }

    private void values_validation() {

        if(!str_age.equalsIgnoreCase("")) {
            profi_age.setText(profile_list.get(0).getAge());
                    } else {
            profi_age.setText("");
                    }

        if(!str_name.equalsIgnoreCase("")) {
            if(str_age.equalsIgnoreCase("")) {
                profi_name.setText(profile_list.get(0).getFullname());
                        } else {
                profi_name.setText(profile_list.get(0).getFullname() + ", ");
                    }
                }


        if(!str_designation.equalsIgnoreCase("")) {
            profi_designation.setText(profile_list.get(0).getJob_position());
                    } else {
            profi_designation.setText("");
                }

        if(!str_about_me.equalsIgnoreCase("")) {
            profi_about_me.setText(profile_list.get(0).getAbout_me());
                } else {
            profi_about_me.setVisibility(View.GONE);
             }

        if(!str_marital_status.equalsIgnoreCase("")) {
            married_status.setText(profile_list.get(0).getMarital_status());
                    } else {
            married_status.setText("");
                }

        if(!str_weight.equalsIgnoreCase("")) {
            profi_weight.setText(profile_list.get(0).getWeight());
                    } else {
            profi_weight.setText("");
                }

        if(!str_height.equalsIgnoreCase("")) {
            profi_height.setText(profile_list.get(0).getHeight());
                    } else {
            profi_height.setText("");
                }

         if(profile_list.get(0).getProfilepicture() != null) {
             if (!profile_list.get(0).getProfilepicture().equalsIgnoreCase("")) {
                 Glide.with(getApplicationContext()).load(profile_list.get(0).getProfilepicture()).into(profi_user_image);
                            }
                    }

         if(profile_list.get(0).getCover_photo_link() != null) {
                if(!profile_list.get(0).getCover_photo_link().equalsIgnoreCase("")) {
                    Glide.with(getApplicationContext()).load(profile_list.get(0).getCover_photo_link()).into(profi_cover_photo);
                            }
                    }
            }

    @Override
    public void onBackPressed() {
        setResult(NEW_HIS_PROFILE_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.profi_user_image:
                if(profile_list.get(0).getProfilepicture() != null) {
                    if (!profile_list.get(0).getProfilepicture().equalsIgnoreCase("")) {
                        new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, profi_user_image,
                                profile_list.get(0).getProfilepicture(), null);
                                }
                            }
                break;
            case R.id.profi_cover_photo:
                if(profile_list.get(0).getCover_photo_link() != null) {
                    if(!profile_list.get(0).getCover_photo_link().equalsIgnoreCase("")) {
                        new PhotoFullPopupWindow(getApplicationContext(), R.layout.popup_photo_full, profi_cover_photo,
                                profile_list.get(0).getCover_photo_link(), null);
                                }
                            }
                break;
            case R.id.chat_link:

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(chat_link, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new get_chat_receive_list(str_user_fb_id, profile_list.get(0).getFb_id()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
                }
            }

    private class get_chat_receive_list extends AsyncTask<Void, Chat_Receive_Response,
            Chat_Receive_Response> {

        String my_fb_id, his_fb_id;

        public get_chat_receive_list(String my_fb_id, String his_fb_id) {
            this.my_fb_id = my_fb_id;
            this.his_fb_id = his_fb_id;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Chat_Receive_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_Receive_Response> call = a.get_receive_chat(my_fb_id, his_fb_id);
            Chat_Receive_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Receive_Resp: " + c.toString());
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
              }

        @Override
        protected void onPostExecute(Chat_Receive_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    chat_receive_list.clear();
                    Stored_datas.getInstance().setChat_his_fb_id(c.to_fb_id);
                    Stored_datas.getInstance().setChat_his_name(c.to_user_name);
                    Stored_datas.getInstance().setChat_his_image(c.to_user_photo);
                    Log.e(DATIFY_LOG, "Chat_Name_Before: " + Stored_datas.getInstance().getChat_his_name());

                    if (null != c.chat_list_results && c.chat_list_results.size() > 0) {
                        for (Chat_Receive_Response.Chat_List_Result dd : c.chat_list_results) {

                            chat_receive_list_model = new Chat_Receive_List(dd.chat_from_fb_id, dd.chat_to_fb_id,
                                    dd.chat_message, dd.chat_date, dd.chat_time);
                            chat_receive_list.add(chat_receive_list_model);

                                }
                            }

                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(chat_receive_list);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(New_His_Profile_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                        } else if (c.status == 0) {
                    Stored_datas.getInstance().setChat_his_fb_id(his_fb_id);
                    Stored_datas.getInstance().setChat_his_name(str_name);
                    Stored_datas.getInstance().setChat_his_image(profile_list.get(0).getProfilepicture());
                    dialog.dismiss();
                    Stored_datas.getInstance().setChat_receive_list(null);
                    Stored_datas.getInstance().setChat_from_status("2");
                    Intent chat_intent = new Intent(New_His_Profile_Screen.this, Chat_Screen.class);
                    startActivityForResult(chat_intent, HIS_PROFILE_to_CHAT);
                        }
                    }}
                }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
                    }
                }
