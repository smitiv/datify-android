package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IntegerRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
//import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.smitiv.datify.Models.Chat_Details_List;
import com.smitiv.datify.Models.Chat_Receive_List;
import com.smitiv.datify.Response.Chat_Send_Response;
import com.smitiv.datify.Sessions.Stored_datas;

import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.R;

import java.io.IOException;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Chat_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id, version;

    ImageView back_arrow;
    Toolbar toolbar;
    TextView chat_person_name;
    CircularImageView chat_user_image;
    RecyclerView chat_recyclerview;
    EditText chat_msg_field;
    ImageView send_chat;
    String chat_his_fb_id;
    String chat_his_name;
    String chat_his_image;
    ArrayList<Chat_Receive_List> chat_receive_list;
    RecyclerView.Adapter received_chat_adapter;
    AlertDialog dialog;
    ArrayList<Chat_Details_List> chat_details_list;
    ArrayList<Chat_Details_List> chat_details_list_temporary;
    String chat_list_position;
    String upd_status = "0";
    Chat_Details_List items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Chat_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
            }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        chat_person_name = (TextView) findViewById(R.id.chat_person_name);
        chat_user_image = (CircularImageView) findViewById(R.id.chat_user_image);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                        }
                    });
                }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Chat_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        editor.commit();

        try {
            PackageInfo pInfo = getPackageManager().
                    getPackageInfo(getPackageName(), 0);
            version  = pInfo.versionName;
            Log.e(DATIFY_LOG,"Package_Version: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void init_views() {
        chat_details_list = new ArrayList<Chat_Details_List>();
        chat_details_list_temporary = new ArrayList<Chat_Details_List>();
        chat_details_list = Stored_datas.getInstance().getChat_details_list();

        if(Stored_datas.getInstance().getChat_from_status().equalsIgnoreCase("1")) {
            if (chat_details_list != null) {
                if (chat_details_list.size() > 0) {
                    chat_details_list_temporary.addAll(chat_details_list);
                           }
                        }
                    }

        chat_list_position = Stored_datas.getInstance().getChat_list_position();

        dialog = new SpotsDialog(Chat_Screen.this);
        chat_recyclerview = (RecyclerView) findViewById(R.id.chat_recyclerview);
        chat_msg_field = (EditText) findViewById(R.id.chat_msg_field);
        send_chat = (ImageView) findViewById(R.id.send_chat);
        send_chat.setOnClickListener(this);
        chat_receive_list = new ArrayList<Chat_Receive_List>();
        chat_receive_list = Stored_datas.getInstance().getChat_receive_list();

        if(chat_receive_list != null) {
            if (chat_receive_list.size() > 0) {
                chat_recyclerview.setHasFixedSize(true);
                LinearLayoutManager manager = new LinearLayoutManager(Chat_Screen.this);    // LinearLayout for Listview
                manager.setStackFromEnd(true);
                chat_recyclerview.setLayoutManager(manager);
                received_chat_adapter = new Received_Chat_Adapter(getApplicationContext(), chat_receive_list);
                chat_recyclerview.setAdapter(received_chat_adapter);
                received_chat_adapter.notifyDataSetChanged();
                     }
                    }

        chat_his_image = Stored_datas.getInstance().getChat_his_image();
        chat_his_fb_id = Stored_datas.getInstance().getChat_his_fb_id();
        chat_his_name = Stored_datas.getInstance().getChat_his_name();

        Glide.with(getApplicationContext()).load(chat_his_image).into(chat_user_image);
        chat_person_name.setText(chat_his_name);
            }

    @Override
    public void onBackPressed() {

        if(Stored_datas.getInstance().getChat_from_status().equalsIgnoreCase("1")) {

            if (upd_status.equalsIgnoreCase("1")) {
                chat_details_list_temporary.remove(Integer.parseInt(chat_list_position));
                chat_details_list_temporary.add(0, items);
                chat_details_list.clear();

                for (int i = 0; i < chat_details_list_temporary.size(); i++) {
                    Chat_Details_List list_items = new Chat_Details_List();
                    list_items.setChat_user_id(chat_details_list_temporary.get(i).getChat_user_id());
                    list_items.setChat_user_name(chat_details_list_temporary.get(i).getChat_user_name());
                    list_items.setChat_message(chat_details_list_temporary.get(i).getChat_message());
                    list_items.setChat_user_photo(chat_details_list_temporary.get(i).getChat_user_photo());
                    list_items.setChat_time(chat_details_list_temporary.get(i).getChat_time());
                    list_items.setChat_date(chat_details_list_temporary.get(i).getChat_date());
                    list_items.setChat_status("sent_msg");
                    chat_details_list.add(list_items);
                            }
                Stored_datas.getInstance().setChat_details_list(chat_details_list);
                           }
                        } else {
                    Log.e(DATIFY_LOG, "Non Chat List Page");
                        }

        setResult(CHAT_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.send_chat:

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(send_chat, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                String str_msg = chat_msg_field.getText().toString().trim();

                if(str_msg.equalsIgnoreCase("")) {
                    Snackbar.make(send_chat, "Please enter your message", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new send_my_message(str_user_fb_id, chat_his_fb_id, str_msg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                break;
                  }
              }

    class Received_Chat_Adapter extends RecyclerView.Adapter<Received_Chat_Adapter.ViewHolder> {
        Context context;
        ArrayList<Chat_Receive_List> chat_receive_list;

        public Received_Chat_Adapter(Context context,  ArrayList<Chat_Receive_List> chat_receive_list) {
            this.context = context;
            this.chat_receive_list = chat_receive_list;
                    }

        @Override
        public Received_Chat_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_items, parent, false);
            return new Received_Chat_Adapter.ViewHolder(view);
                    }

        @Override
        public void onBindViewHolder(Received_Chat_Adapter.ViewHolder holder, final int position) {
            String shown_date = "";
            String str_date = chat_receive_list.get(position).getChat_date();
            if(str_date != null) {
                if(!str_date.equalsIgnoreCase("")) {
                    String[] c_in_array = str_date.split("-");
                    String picked_date = c_in_array[2];
                    String picked_month = c_in_array[1];
                    String picked_year = c_in_array[0];

                    String month_name = get_month_name(picked_month);
                    shown_date = picked_date + " " + month_name + ", " + picked_year;
                        }
                    }
            holder.current_date.setText(shown_date);
            if(position > 0) {
                if(str_date.equalsIgnoreCase(chat_receive_list.get(position - 1).getChat_date())) {
                    holder.current_date.setVisibility(View.GONE);
                            } else {
                    holder.current_date.setVisibility(View.VISIBLE);
                        }
                    }

            if(str_user_fb_id.equalsIgnoreCase(chat_receive_list.get(position).getChat_from_fb_id())) {
                holder.his_layout.setVisibility(View.GONE);
                holder.my_msg.setText(chat_receive_list.get(position).getChat_message());
                holder.my_msg_time.setText(chat_receive_list.get(position).getChat_time());
                            } else {
                holder.my_layout.setVisibility(View.GONE);
                holder.his_msg.setText(chat_receive_list.get(position).getChat_message());
                holder.his_msg_time.setText(chat_receive_list.get(position).getChat_time());
                        }
                    }

        @Override
        public int getItemCount() {
            return chat_receive_list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout his_layout, my_layout;
            TextView his_msg, his_msg_time;
            TextView my_msg, my_msg_time, current_date;

            public ViewHolder(View itemView) {
                super(itemView);
                current_date = (TextView) itemView.findViewById(R.id.current_date);
                his_layout = (RelativeLayout)itemView.findViewById(R.id.his_layout);
                my_layout = (RelativeLayout)itemView.findViewById(R.id.my_layout);

                his_msg = (TextView)itemView.findViewById(R.id.his_msg);
                his_msg_time = (TextView)itemView.findViewById(R.id.his_msg_time);
                my_msg = (TextView)itemView.findViewById(R.id.my_msg);
                my_msg_time = (TextView)itemView.findViewById(R.id.my_msg_time);
                         }
                     }
                }

    public static String get_month_name(String month_val) {

        int month_value = Integer.parseInt(month_val) - 1;

        ArrayList<String> months_str = new ArrayList<String>();
        months_str.add("JAN");
        months_str.add("FEB");
        months_str.add("MAR");
        months_str.add("APR");
        months_str.add("MAY");
        months_str.add("JUN");
        months_str.add("JUL");
        months_str.add("AUG");
        months_str.add("SEP");
        months_str.add("OCT");
        months_str.add("NOV");
        months_str.add("DEC");

        return months_str.get(month_value);
            }

    private class send_my_message extends AsyncTask<Void, Chat_Send_Response,
            Chat_Send_Response> {

        String my_fb_id, his_fb_id, chat_msg;

        public send_my_message(String my_fb_id, String his_fb_id, String chat_msg) {
            this.my_fb_id = my_fb_id;
            this.his_fb_id = his_fb_id;
            this.chat_msg = chat_msg;
                    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Chat_Send_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Chat_Send_Response> call = a.chat_send_msg(my_fb_id, his_fb_id, chat_msg);
            Chat_Send_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG,"Filtered_Resp: " + c.toString());
                    } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
                }

        @Override
        protected void onPostExecute(Chat_Send_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {

                    upd_status = "1";
                    items = new Chat_Details_List();
                    items.setChat_user_id(c.to_fb_id);
                    items.setChat_user_name(chat_his_name);
                    items.setChat_user_photo(chat_his_image);
                    items.setChat_message(c.chat_message);
                    items.setChat_date(c.chat_date);
                    items.setChat_time(c.chat_time);
                    items.setChat_status("sent_msg");

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    if(chat_receive_list == null) {
                        chat_receive_list = new ArrayList<Chat_Receive_List>();
                                }

                    Chat_Receive_List items = new Chat_Receive_List();
                    items.setChat_from_fb_id(c.from_fb_id);
                    items.setChat_to_fb_id(c.to_fb_id);
                    items.setChat_message(c.chat_message);
                    items.setChat_time(c.chat_time);
                    items.setChat_date(c.chat_date);
                    chat_receive_list.add(items);

                        chat_recyclerview.setHasFixedSize(true);
                        LinearLayoutManager manager = new LinearLayoutManager(Chat_Screen.this);    // LinearLayout for Listview
                        manager.setStackFromEnd(true);
                        chat_recyclerview.setLayoutManager(manager);
                        received_chat_adapter = new Received_Chat_Adapter(getApplicationContext(), chat_receive_list);
                        chat_recyclerview.setAdapter(received_chat_adapter);

                        received_chat_adapter.notifyDataSetChanged();
                        chat_msg_field.setText("");
                        dialog.dismiss();

                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Chat_Screen.this, c.msg , Toast.LENGTH_LONG).show();
                            }
                        }}
                    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }
}
