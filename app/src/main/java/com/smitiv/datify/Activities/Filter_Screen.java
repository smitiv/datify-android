package com.smitiv.datify.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.materialrangebar.RangeBar;
import com.bumptech.glide.Glide;
import com.smitiv.datify.API_Calls.APIService;
import com.smitiv.datify.API_Calls.APIUrl;
import com.smitiv.datify.Dashboard_Classes.Dashboard;
import com.smitiv.datify.Intent_Constants.Intent_Constants;
import com.smitiv.datify.Internet_Connection.ConnectionDetector;
import com.smitiv.datify.R;
import com.smitiv.datify.Response.Filter_Response;
import com.smitiv.datify.Response.Login_Response;

import java.io.IOException;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Filter_Screen extends AppCompatActivity implements Intent_Constants, View.OnClickListener {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Retrofit retrofit_obj;
    String mess;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    RangeBar age_range_bar, distance_range_bar;
    String str_user_name, str_user_email, str_user_profile_img, str_user_dob, str_user_fb_id;
    TextView start_age_value, end_age_value;

    TextView start_distance_value;
    ImageView message_alert, requests_alert, profile_visit_alerts;
    String msg_alert = "", request_alert = "", p_visit_alert = "";

    ImageView back_arrow;
    private Toolbar toolbar;

    String age_filter_value="", distance_filter_value="", alerts_filter_value = "";
    String init_start_age = "", init_end_age = "";
    int msg_status = 0, request_status = 0, p_visits = 0;
    LinearLayout filter_update_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filters_layout);

        overridePendingTransition(R.anim.open_start, R.anim.open_end);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        pref = PreferenceManager.getDefaultSharedPreferences(Filter_Screen.this);

        toolbar_init();
        pref_validation();
        init_views();
        alerts_validation();
             }

    private void alerts_validation() {
        if(alerts_filter_value.contains("Messages")) {
            msg_status = 1;
            message_alert.setImageResource(R.drawable.msg_filled_layout);
            msg_alert = "Messages";
                    }
        if(alerts_filter_value.contains("Request")) {
            request_status = 1;
            requests_alert.setImageResource(R.drawable.msg_filled_layout);
            request_alert = "Request";
                    }
        if(alerts_filter_value.contains("Profile Visits")) {
            p_visits = 1;
            profile_visit_alerts.setImageResource(R.drawable.msg_filled_layout);
            p_visit_alert = "Profile Visits";
                    }
                }

    private void toolbar_init() {
        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        back_arrow = (ImageView) toolbar.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                   }
                });
            }

    private void pref_validation() {
        pref = PreferenceManager.getDefaultSharedPreferences(Filter_Screen.this);
        editor = pref.edit();
        str_user_fb_id = pref.getString("datify_id", "");
        str_user_name = pref.getString("datify_name", "");
        str_user_email = pref.getString("datify_email", "");
        str_user_profile_img = pref.getString("datify_profile_img", "");
        str_user_dob = pref.getString("datify_dob", "");
        age_filter_value = pref.getString("datify_looking_for", "");
        distance_filter_value = pref.getString("datify_distance", "");
        alerts_filter_value = pref.getString("datify_alerts", "");
        editor.commit();

        if(age_filter_value.contains("-")) {
            String[] age_array = age_filter_value.split("-");
            init_start_age = age_array[0];
            init_end_age = age_array[1];
                    }
                }

    private void init_views() {
        age_range_bar = (RangeBar) findViewById(R.id.age_range_bar);
        distance_range_bar = (RangeBar) findViewById(R.id.distance_range_bar);

        start_distance_value = (TextView) findViewById(R.id.start_distance_value);
        start_age_value = (TextView) findViewById(R.id.start_age_value);
        end_age_value = (TextView) findViewById(R.id.end_age_value);

        message_alert = (ImageView) findViewById(R.id.message_alert);
        requests_alert = (ImageView) findViewById(R.id.requests_alert);
        profile_visit_alerts = (ImageView) findViewById(R.id.profile_visit_alerts);
        filter_update_layout = (LinearLayout) findViewById(R.id.filter_update_layout);
        message_alert.setOnClickListener(this);
        requests_alert.setOnClickListener(this);
        profile_visit_alerts.setOnClickListener(this);
        filter_update_layout.setOnClickListener(this);

        if(!init_start_age.equalsIgnoreCase("") && !init_end_age.equalsIgnoreCase("")) {
            Float start_range_value = Float.parseFloat(init_start_age);
            Float end_range_value = Float.parseFloat(init_end_age);
            age_range_bar.setRangePinsByValue(start_range_value, end_range_value);

            start_age_value.setText(init_start_age + " years");
            end_age_value.setText(init_end_age + " years");
                        }

        if(!distance_filter_value.equalsIgnoreCase("")) {
            Float distance_float = Float.parseFloat(distance_filter_value);
            distance_range_bar.setRangePinsByValue(distance_float, distance_float);
                        }

        distance_range_bar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {

                Log.e(DATIFY_LOG , "RightPin: " + rightPinValue);
                Log.e(DATIFY_LOG , "LeftPin: " + leftPinValue);
                start_distance_value.setText(rightPinValue + " km");
                distance_filter_value = rightPinValue;
                            }
                        });

        age_range_bar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {

                int original_min_range = leftPinIndex + 18;
                int original_max_range = rightPinIndex + 18;

                start_age_value.setText(original_min_range + " years");
                end_age_value.setText(original_max_range + " years");
                age_filter_value = original_min_range + "-" + original_max_range;
                            }
                        });
                    }

    @Override
    public void onBackPressed() {
        setResult(FILTER_back);
        finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
            }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.message_alert:
                if(msg_status == 1) {
                    message_alert.setImageResource(R.drawable.msg_empty_layout);
                    msg_status = 0;
                    msg_alert = "";
                        } else {
                    message_alert.setImageResource(R.drawable.msg_filled_layout);
                    msg_status = 1;
                    msg_alert = "Messages";
                     }
                break;
            case R.id.requests_alert:
                if(request_status == 1) {
                    requests_alert.setImageResource(R.drawable.msg_empty_layout);
                    request_status = 0;
                    request_alert = "";
                            } else {
                    requests_alert.setImageResource(R.drawable.msg_filled_layout);
                    request_status = 1;
                    request_alert = "Request";
                        }
                break;
            case R.id.profile_visit_alerts:
                if(p_visits == 1) {
                    profile_visit_alerts.setImageResource(R.drawable.msg_empty_layout);
                    p_visits = 0;
                    p_visit_alert = "";
                            } else {
                    profile_visit_alerts.setImageResource(R.drawable.msg_filled_layout);
                    p_visits = 1;
                    p_visit_alert = "Profile Visits";
                        }
                break;
            case R.id.filter_update_layout:
                if(age_filter_value.equalsIgnoreCase("")) {
                    show_message("Please pick age limit.");
                    return;
                       }

                if(distance_filter_value.equalsIgnoreCase("")) {
                    show_message("Please pick distance limit.");
                    return;
                       }
                String noti_value = "";

                if(!msg_alert.equalsIgnoreCase("")) {
                    noti_value = "Messages";
                        }
                if(!request_alert.equalsIgnoreCase("")) {
                    if(!noti_value.equalsIgnoreCase("")) {
                        noti_value = noti_value.concat(", Request");
                                } else {
                        noti_value = "Request";
                            }
                        }

                if(!p_visit_alert.equalsIgnoreCase("")) {
                    if(!noti_value.equalsIgnoreCase("")) {
                        noti_value = noti_value.concat(", Profile Visits");
                                } else {
                        noti_value = "Profile Visits";
                            }
                        }

                alerts_filter_value = noti_value;
                 Log.e(DATIFY_LOG, "Age: " + age_filter_value);
                 Log.e(DATIFY_LOG, "Distance: " + distance_filter_value);
                 Log.e(DATIFY_LOG, "Alerts: " + noti_value);

                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    Snackbar.make(filter_update_layout, "No Internet Connection", Snackbar.LENGTH_LONG).
                            setAction("Action", null).show();
                    return;
                        }

                mess = APIUrl.BASE_URL;
                retrofit_obj = new Retrofit.Builder()
                        .baseUrl(mess)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                new update_filter(str_user_fb_id, age_filter_value, distance_filter_value,
                        alerts_filter_value).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
                }
            }

    private void show_message(String str) {
    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
                    }

    private class update_filter extends AsyncTask<Void, Filter_Response,
            Filter_Response> {

        AlertDialog dialog = new SpotsDialog(Filter_Screen.this);
        String fb_id, age, distance, alerts;

        public update_filter(String fb_id, String age, String distance, String alerts) {
            this.fb_id = fb_id;
            this.age = age;
            this.distance = distance;
            this.alerts = alerts;
                }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
                }

        @Override
        protected Filter_Response doInBackground(Void... params) {
            APIService a = retrofit_obj.create(APIService.class);
            Call<Filter_Response> call = a.update_filter(fb_id, age, distance, alerts);
            Filter_Response c = null;
            try {
                c = call.execute().body();
                Log.e(DATIFY_LOG, "Filter_Response: " + c.toString());
                        } catch (IOException e) {
                e.printStackTrace();
                    }
            return c;
            }

        @Override
        protected void onPostExecute(Filter_Response c) {
            super.onPostExecute(c);
            String anullcheck = c != null ? "Yes"
                    : "no";
            if (anullcheck.equals("Yes")) {
                if (c.status == 1) {
                    dialog.dismiss();

                    editor = pref.edit();
                    editor.putString("datify_looking_for", c.looking_for);
                    editor.putString("datify_distance", c.distance);
                    editor.putString("datify_alerts", c.alerts);
                    editor.commit();

                    Log.e(DATIFY_LOG, "Updated_Looking_for: " + c.looking_for);
                    Log.e(DATIFY_LOG, "Updated_Distance: " + c.distance);
                    Log.e(DATIFY_LOG, "Updated_Alerts: " + c.alerts);

                    Toast.makeText(getApplicationContext(), c.msg, Toast.LENGTH_SHORT).show();
                    setResult(FILTER_updated);
                    finish();
                        } else if (c.status == 0) {
                    dialog.dismiss();
                    Toast.makeText(Filter_Screen.this, c.msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
}
