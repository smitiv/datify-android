package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Request_received_Response {

    public int status;
    public String msg;

    @SerializedName("invited_list")
    public List<Request_received_Result> request_received_result;

    public class Request_received_Result {

        public String from_fb_id, fullname, email, profilepicture, gender, dob, job_position, age;
                    }

            }
