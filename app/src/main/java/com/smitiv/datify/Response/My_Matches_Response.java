package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class My_Matches_Response {

    public int status;
    public String msg;

    @SerializedName("my_matches_list")
    public List<My_Matches_Result> my_matches_result;

    public class My_Matches_Result {
        public String user_fb_id, fullname, email, profilepicture, gender, dob, looking_for, distance, alerts,
                about_me, education, marital_status, interest, location, job_position, age, weight, height,
                distance_in_km;
                    }
            }
