package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Chat_Receive_Response {

    public int status;
    public String msg;
    public String from_fb_id;
    public String to_fb_id;
    public String to_user_name;
    public String to_user_photo;

    @SerializedName("chat_list")
    public List<Chat_List_Result> chat_list_results;

    public class Chat_List_Result {
        public String chat_from_fb_id, chat_to_fb_id, chat_message, chat_date, chat_time;
                    }

            }
