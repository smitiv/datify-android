package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Request_sent_Response {

    public int status;
    public String msg;

    @SerializedName("request_sent_users_list")
    public List<Request_sent_Result> request_sent_result;

    public class Request_sent_Result {

        public String user_fb_id, fullname, email, profilepicture, gender, dob, job_position, age;
                    }

            }
