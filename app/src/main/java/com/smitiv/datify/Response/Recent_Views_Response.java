package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Recent_Views_Response {

    public int status;
    public String msg;

    @SerializedName("recent_views")
    public List<Recent_Views_Result> recent_views_results;

    public class Recent_Views_Result {
        public String user_fb_id, fullname, email, profilepicture, job_position, age, viewed_date;
                    }

            }
