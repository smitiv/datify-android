package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Chat_List_Response {

    public int status;
    public String msg;

    @SerializedName("chat_user_list")
    public List<Chat_List_Result> chat_list_result;

    public class Chat_List_Result {
        public String chat_user_id, chat_user_name, chat_user_photo, chat_message, chat_date, chat_time, chat_status;
                    }

            }
