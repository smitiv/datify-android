package com.smitiv.datify.Response;

/**
 * Created by govindaraj on 15/05/18.
 */

public class Edit_Profile_Response {

    public int status;
    public String msg;
    public String id;
    public String fb_id;
    public String fullname;
    public String email;
    public String profilepicture;
    public String gender;
    public String dob;
    public String looking_for;
    public String distance;
    public String alerts;
    public String about_me;
    public String education;
    public String marital_status;
    public String interest;
    public String location;
    public String job_position;
    public String age;
    public String weight;
    public String height;
            }
