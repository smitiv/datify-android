package com.smitiv.datify.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by govindaraj on 16/02/18.
 */

public class Filtered_Matches_Response {

    public int status;
    public String msg;
    public String fb_id;
    public String srch_distance;
    public String srch_looking_for;
    public String srch_gender;

    @SerializedName("matches_list")
    public List<Filtered_Match_Result> filtered_match_results;

    public class Filtered_Match_Result {
        public String user_fb_id, fullname, email, profilepicture, gender, dob, looking_for, distance, alerts,
                about_me, education, marital_status, interest, location, job_position, age, weight, height,
                distance_in_km;
                    }

            }
